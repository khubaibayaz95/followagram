const express = require('express')
const router = express.Router()

const mongoose = require('mongoose')
const User = mongoose.model("User")
const crypto = require('crypto')
const bcrypt = require("bcryptjs")
const jwt = require('jsonwebtoken')
const {JWT_SECRET} = require('../config/keys')
const requireLogin = require('../middleware/requireLogin')
const nodemailer = require('nodemailer')
const sendgridTransport = require('nodemailer-sendgrid-transport')
const {EMAIL_KEY} = require('../config/keys')
const {SITE_URL} = require('../config/keys')

const transporter = nodemailer.createTransport(sendgridTransport({
    auth: {
        api_key: EMAIL_KEY
    }
}))


router.post('/signup', (req, res) => {

    const { name, email, password, picture } = req.body
    if (!email || !password || !name) {
        return res.status(422).json({ error: "Please enter all the details" })
    }
    User.findOne({ email: email })
        .then((savedUser) => {
            if (savedUser) {
                return res.status(422).json({ error: "User already exist with that email" })
            }

            bcrypt.hash(password, 12)
                .then((hashedPassword) => {
                    const user = new User({
                        email,
                        password: hashedPassword,
                        name,
                        picture
                    })
        
                    user.save()
                        .then((user) => {
                            // transporter.sendMail({
                            //     to: user.email,
                            //     from: 'info@sbwservices.co.uk',
                            //     subject: 'Welcome - Signedup Successfully',
                            //     html: '<h3> Welcome ' + name + ' to Followagram </h3>'
                            // })
                            // .then(() => {
                            //     // if we want to do anything after sending the email   
                            //     // code can be done later 
                            // })

                            res.json({ message: "User signed up successfully" })
                        })
                        .catch((err) => {
                            return res.status(422).json({ error: err })
                        });
                })
        })
        .catch((err) => {
            return res.status(422).json({ error: err })
        });
})

router.get('/protected', requireLogin, (req, res) => {
    res.send('Hello user')
})

router.post('/signin', (req, res) => {
    const { email, password } = req.body

    if (!email || !password) {
        return res.json({ error: "Please add email or password", status: 422 })
    }

    User.findOne({ email: email })
        .then((savedUser) => {
            if(!savedUser) {
                return res.json({ error: "Invalid email or password", status: 422 })
            }

            bcrypt.compare(password, savedUser.password)
                .then((doMatch) => {
                    if(doMatch) {
                        let token = jwt.sign({_id: savedUser._id}, JWT_SECRET) 
                        let data = {
                            _id: savedUser._id,
                            email: savedUser.email,
                            name: savedUser.name,
                            picture: savedUser.picture,
                            followers: savedUser.followers,
                            following: savedUser.following,
                            followersRequests: savedUser.followersRequests,
                            followingRequests: savedUser.followingRequests,
                            settingsId: savedUser.settingsId ? savedUser.settingsId : ""
                        }


                        return res.json({ message: "OK", user: data, token })
                    }
                    else {
                        return res.json({ error: "Invalid email or password", status: 200 })
                    }
                })
                .catch((err) => {
                    console.log(err)
                })
        })
        .catch((err) => {
            console.log(err)
        })
});

router.post('/reset-password', (req, res) => {
    crypto.randomBytes(32, (err, buffer) => {
        if(err) {
            console.log(err)
        }

        let token = buffer.toString('hex')

        User.findOne({email: req.body.email})
            .then(user => {
                if(!user) {
                    return res.status(422).json({error: 'User doesn\'t exist with the given email'})
                }

                user.resetToken = token;
                user.password = user.password;
                user.expireToken = Date.now() + 3600000;
                user.save().then((result) => {
                    transporter.sendMail({
                        to: user.email,
                        from: 'info@sbwservices.co.uk',
                        subject: 'Reset Password',
                        html: `
                        <p>You requested for password reset</p>
                        <h5>Click on this <a target="_blank" href="${SITE_URL}/reset-password/${token}">link</a> to reset password</h5>
                        <p>If you haven't requested for password reset then ignore this email or change your password for better security</p>
                        `
                    })

                    res.json({ message: "OK", data: 'Kindly check your email for resetting the password'})
                })


            })

    })
})

router.post('/new-password', (req, res) => {
    const newPassword = req.body.password
    const sentToken = req.body.token

    User.findOne({resetToken: sentToken, expireToken:{$gt: Date.now()}})
    .then(user => {
        if(!user) {
            return res.status(422).json({error: 'Link expired. Kindly reset password again.'})
        }

        bcrypt.hash(newPassword, 12).then(hashedPassword => {
            user.password = hashedPassword
            user.resetToken = undefined
            user.expireToken = undefined

            user.save()
            .then((savedUser) => {
                res.json({message: 'OK', data: 'Password updated successfully'})
            })
        })
    })
    .catch(err => {
        console.log(err)
    })
})

module.exports = router;