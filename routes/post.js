
const express = require('express')
const router = express.Router()

const mongoose = require('mongoose')
const requireLogin = require('../middleware/requireLogin')
const Post = mongoose.model("Post")
const User = mongoose.model("User")

router.get('/allpost', requireLogin, (req, res) => {
    Post.find()
        .populate("postedBy", "_id name picture commentLikedBy")   // for expanding record instead of just showing user id (showing only _id and name)
        .populate("comments.postedBy", "_id name picture")
        .populate("likes", "_id name picture")
        .sort('-createdAt')    // - will sort by descending
        .then(posts => {
            res.json({ message: "Ok", data: posts })
        })
        .catch(err => {
            console.log(err)
        })
});

router.post('/createpost', requireLogin, (req, res) => {
    const { title, body, picture } = req.body

    if (!title || !body || !picture) {
        return res.status(422).json({ error: "Please add all the required fields" })
    }

    const post = new Post({
        title,
        body,
        picture,
        postedBy: req.user
    })


    post.save()
        .then(savedPost => {
            return res.json({ message: "Ok", data: savedPost })
        })
        .catch(err => {
            console.log(err)
        })
})

router.get('/mypost', requireLogin, async (req, res) => {

    let userDetails;
    await fetchUserDetails(req.user._id, function (err, response) {
        if (response) {
            userDetails = response
        }
    })

    Post.find({ postedBy: req.user._id })
        .populate("postedBy", "_id name")   // for expanding record instead of just showing user id (showing only _id and name)
        .sort('-createdAt')    // - will sort by descending
        .then(posts => {
            res.json({ message: "Ok", data: posts, userDetails })
        })
        .catch(err => {
            res.status(422).json({ error: err })
        })
})

router.put('/like', requireLogin, (req, res) => {
    Post.findByIdAndUpdate(req.body.postId, {
        $push: { likes: req.user._id }
    }, {
        new: true    // to send updated new record
    })
        .populate("comments.postedBy", "_id name picture")
        .populate("postedBy", "_id name picture")
        .populate("likes", "_id name picture")
        .exec((err, result) => {
            if (err) {
                return res.status(422).json({ error: err })
            }
            else {
                return res.json({ message: "OK", data: result })
            }
        })
})

router.put('/unlike', requireLogin, (req, res) => {
    Post.findByIdAndUpdate(req.body.postId, {
        $pull: { likes: req.user._id }
    }, {
        new: true    // to send updated new record
    })
        .populate("comments.postedBy", "_id name picture")
        .populate("postedBy", "_id name picture")
        .populate("likes", "_id name picture")
        .exec((err, result) => {
            if (err) {
                return res.status(422).json({ error: err })
            }
            else {
                return res.json({ message: "OK", data: result })
            }
        })
})

router.put('/comment', requireLogin, (req, res) => {
    const comment = {
        text: req.body.text,
        postedBy: req.user._id
    }
    Post.findByIdAndUpdate(req.body.postId, {
        $push: { comments: comment }
    }, {
        new: true    // to send updated new record
    })
        .populate("comments.postedBy", "_id name picture")
        .populate("postedBy", "_id name picture")
        .exec((err, result) => {
            if (err) {
                return res.status(422).json({ error: err })
            }
            else {
                return res.json({ message: "OK", data: result })
            }
        })
})

router.delete('/deletepost/:postId', requireLogin, (req, res) => {

    Post.findOne({ _id: req.params.postId })
        .populate('postedBy', '_id')
        .exec((err, post) => {
            if (err || !post) {
                return res.status(422).json({ error: err })
            }
            if (post.postedBy._id.toString() === req.user._id.toString()) {
                post.remove()
                    .then(result => {
                        return res.json({ message: "Post deleted successfully", data: result })
                    })
                    .catch(err => {
                        console.log(err)
                    })

            }
        })
})

router.get('/getfolloweduserpost', requireLogin, (req, res) => {

    Post.find({
        postedBy: {
            $in: req.user.following
        }
    })
        .populate("postedBy", "_id name picture")   // for expanding record instead of just showing user id (showing only _id, pic and name)
        .populate("comments.postedBy", "_id name commentLikedBy picture")
        .populate("likes", "_id name picture")
        .sort('-createdAt')    // - will sort by descending
        .then(posts => {
            res.json({ message: "Ok", data: posts })
        })
        .catch(err => {
            console.log(err)
        })
});

router.put('/like-comment', requireLogin, (req, res) => {
    Post.updateOne({ '_id': req.body.postId, 'comments._id': req.body.commentId },
        { $push: { 'comments.$.commentLikedBy': req.user._id }})
        .exec((err, response) => {
            if (err) {
                return res.status(422).json({ error: err })
            }
            else {
                Post.findById(req.body.postId)
                    .populate("comments.postedBy","_id name commentLikedBy picture")
                    .populate("postedBy", "_id name picture")
                    .populate("likes", "_id name picture")
                    .exec((err, result) => {
                        if(err) {
                            return res.status(422).json({error: err})
                        }
                        else {
                            return res.json({message: "OK", data: result})
                        }
                    })
            }
        })
})

router.put('/unlike-comment', requireLogin, (req, res) => {
    Post.updateOne({ '_id': req.body.postId, 'comments._id': req.body.commentId },
        { $pull: { 'comments.$.commentLikedBy': req.user._id }})
        .exec((err, response) => {
            if (err) {
                return res.status(422).json({ error: err })
            }
            else {
                Post.findById(req.body.postId)
                    .populate("comments.postedBy","_id name picture commentLikedBy")
                    .populate("postedBy", "_id name picture")
                    .populate("likes", "_id name picture")
                    .exec((err, result) => {
                        if(err) {
                            return res.status(422).json({error: err})
                        }
                        else {
                            return res.json({message: "OK", data: result})
                        }
                    })
            }
        })
})

router.put('/delete-comment', requireLogin, (req, res) => {
    Post.findByIdAndUpdate(req.body.postId, {
        $pull: { comments: { _id: req.body.commentId } }
    })
    .exec((err, result) => {
        if (err) {
            return res.status(422).json({ error: err })
        }
        else {
            Post.findById(req.body.postId)
            .populate("comments.postedBy","_id name commentLikedBy picture")
            .populate("postedBy", "_id name picture")
            .populate("likes", "_id name picture")
            .exec((err, result) => {
                if(err) {
                    return res.status(422).json({error: err})
                }
                else {
                    return res.json({message: "OK", data: result})
                }
            })
        }
    })
})


async function fetchUserDetails(userId, callback) {
    await User.findById({ '_id': userId })
        .then((savedUser) => {
            if (!savedUser) {
                return callback(null)
            }

            let userDetails = {
                picture: savedUser.picture,
                followers: savedUser.followers,
                following: savedUser.following,
            }
            return callback(null, userDetails)
        })
        .catch((err) => {
            return callback(null)
        })
}


module.exports = router;

