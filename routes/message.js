
const express = require('express')
const router = express.Router()

const mongoose = require('mongoose')
const requireLogin = require('../middleware/requireLogin')
const Message = mongoose.model("Message")


//send Message
router.post("/message/sendMessage", requireLogin, async (req, res) => {

    try {

        const { sender, text, conversationId } = req.body

        const newMessage = new Message({
            sender,
            text,
            conversationId
        })

        //const newMessage = new Message(req.body)

        await newMessage.save()
        .then(savedMsg => {
            return res.json({ message: "Ok", data: savedMsg })
        })
        .catch(err => {
            res.status(422).json({ error: err })
        })
    }
    catch(err) {
        res.status(422).json({ error: err })
    }
})


//get message of specific conversation

router.get("/message/get-msg-from-conv/:conversationId", async (req, res) => {
    try {
        await Message.find({
            conversationId: req.params.conversationId,
        })
        .then(messages => {
            return res.json({ message: "Ok", data: messages })
        })
        .catch(err => {
            console.log(err)
            res.status(422).json({ error: err })
        })
    }
    catch(err) {
        res.status(422).json({ error: err })
    }
})


module.exports = router;
