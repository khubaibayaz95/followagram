const express = require('express')
const router = express.Router()

const mongoose = require('mongoose')
const requireLogin = require('../middleware/requireLogin')
const Post = mongoose.model("Post")
const User = mongoose.model("User")

router.get('/user/:id', requireLogin, (req, res) => {

    User.findOne({ _id: req.params.id })
        .select('-password')   // will send all fields except passwords
        .then(user => {
            Post.find({ postedBy: req.params.id })
                .populate("postedBy", "_id name")   // for expanding record instead of just showing user id (showing only _id and name)
                .exec((err, posts) => {
                    if (err) {
                        return res.status(422).json({ error: err })
                    }
                    res.json({ message: "Ok", user, posts })
                })
        })
        .catch(err => {
            return res.status(404).json({ error: 'User not found' })
        })
})

router.put('/follow', requireLogin, (req, res) => {

    let follId = req.body.followId;
    let currUserId = req.user._id.toString()
    User.findByIdAndUpdate(req.body.followId,
        { $push: { 'followers': req.user._id } },
        { new: true },
        (err, result) => {
            if (err) {
                return res.status(422).json({ error: err })
            }

            User.findByIdAndUpdate(req.user._id,
                { $push: { 'following': req.body.followId } },
                { new: true })
                .select('-password')
                .then(result => {
                    res.json({ message: "OK", result })
                })
                .catch(error => {
                    return res.status(404).json({ error })
                })
        })
})

router.put('/unfollow', requireLogin, (req, res) => {

    User.findByIdAndUpdate(req.body.unfollowId,
        { $pull: { 'followers': req.user._id } },
        { new: true },
        (err, result) => {
            if (err) {
                return res.status(422).json({ error: err })
            }

            User.findByIdAndUpdate(req.user._id,
                { $pull: { 'following': req.body.unfollowId } },
                { new: true })
                .select('-password')
                .then(result => {
                    res.json({ message: "OK", result })
                })
                .catch(error => {
                    return res.status(404).json({ error })
                })
        })
})

router.put('/updateprofilepicture', requireLogin, (req, res) => {

    User.findByIdAndUpdate(req.user._id, {
        $set: { picture: req.body.picture }
    },
        { new: true },
        (err, result) => {
            if (err) {
                return res.status(422).json({ error: err })
            }
            return res.json({ message: "OK", data: result })
        }
    )
})

router.post('/search-users', requireLogin, (req, res) => {
    let userPattern = new RegExp('^' + req.body.query)

    User.find({ 'email': { $regex: userPattern }, '_id': { $ne: req.user._id } })
        .select('_id email picture')
        .then((user) => {
            // if(!user) {
            //     return res.status(422).json({ error: 'No users found' })
            // }  
            res.json({ message: 'OK', data: user })
        })
        .catch(err => {
            console.log(err)
        })
})

router.get('/get-followers-list-user/:id', requireLogin, (req, res) => {
    User.findOne({ _id: req.params.id })
        .select('-following -name -email -password -picture')
        .populate('followers', '_id name email picture')
        .then(user => {
            if (!user) {
                return res.status(422).json({ error: 'No followers found' })
            }
            res.json({ message: "Ok", user })
        })
        .catch(err => {
            return res.status(404).json({ error: 'User not found' })
        })
})

router.get('/get-following-list-user/:id', requireLogin, (req, res) => {
    User.findOne({ _id: req.params.id })
        .select('-followers -name -email -password -picture')
        .populate('following', '_id name email picture')
        .then(user => {
            if (!user) {
                return res.status(422).json({ error: 'No followers found' })
            }
            res.json({ message: "Ok", user })
        })
        .catch(err => {
            return res.status(404).json({ error: 'User not found' })
        })
})

router.put('/send-follow-request', requireLogin, (req, res) => {
    User.findByIdAndUpdate(req.body.followId,
        { $push: { 'followersRequests': req.user._id } },
        { new: true })
        .select('-password -followers -following -name -email -_id')
        .exec((err, results) => {
            if (err) {
                return res.status(422).json({ error: err })
            }
            //res.json({message: "OK", data: result})
            User.findByIdAndUpdate(req.user._id,
                { $push: { 'followingRequests': req.body.followId } },
                { new: true })
                .select('-password -followers -following -name -email -_id')
                .then(result => {
                    res.json({ message: "OK", results, result })
                })
                .catch(error => {
                    return res.status(404).json({ error })
                })
        })
})

router.put('/cancel-follow-request', requireLogin, (req, res) => {

    User.findByIdAndUpdate(req.body.followId,
        { $pull: { 'followersRequests': req.user._id } },
        { new: true })
        .select('-password -followers -following -name -email -_id')
        .exec((err, results) => {
            if (err) {
                return res.status(422).json({ error: err })
            }
            //res.json({message: "OK", data: result})
            User.findByIdAndUpdate(req.user._id,
                { $pull: { 'followingRequests': req.body.followId } },
                { new: true })
                .select('-password -followers -following -name -email -_id')
                .then(result => {
                    res.json({ message: "OK", results, result })
                })
                .catch(error => {
                    return res.status(404).json({ error })
                })
        })
})

router.put('/reject-follow-request', requireLogin, (req, res) => {

    User.findByIdAndUpdate(req.body.followId,
        { $pull: { 'followingRequests': req.user._id } },
        { new: true })
        .select('-password -followers -following -name -email -_id')
        .exec((err, results) => {
            if (err) {
                return res.status(422).json({ error: err })
            }
            //res.json({message: "OK", data: result})
            User.findByIdAndUpdate(req.user._id,
                { $pull: { 'followersRequests': req.body.followId } },
                { new: true })
                .select('-password -followers -following -name -email -_id')
                .then(result => {
                    res.json({ message: "OK", results, result })
                })
                .catch(error => {
                    return res.status(404).json({ error })
                })
        })
})

router.put('/accept-follow-request', requireLogin, (req, res) => {

    User.findByIdAndUpdate(req.body.followId,
        { $pull: { 'followingRequests': req.user._id } },
        { new: true })
        .exec((e, r) => {
            if (e) {
                return res.status(422).json({ error: e })
            }
            User.findByIdAndUpdate(req.body.followId,
                { $push: { 'following': req.user._id } },
                { new: true })
                .select('-password -followers -following -name -email -_id')
                .exec((err, results) => {
                    if (err) {
                        return res.status(422).json({ error: err })
                    }
                    User.findByIdAndUpdate(req.user._id,
                        { $pull: { 'followersRequests': req.body.followId } },
                        { new: true })
                        .exec((errr, resu) => {
                            if (errr) {
                                return res.status(422).json({ error: errr })
                            }
                            User.findByIdAndUpdate(req.user._id,
                                { $push: { 'followers': req.body.followId } },
                                { new: true })
                                .select('-password -following -name -email -_id')
                                .then(result => {
                                    res.json({ message: "OK", results, result })
                                })
                                .catch(error => {
                                    return res.status(404).json({ error })
                                })

                        })
                })
        })
});

router.get('/get-followers-requests/:userId', requireLogin, (req, res) => {

    User.findById({ '_id': req.params.userId })
        .select("id name email")
        .populate("followersRequests", "_id name picture")
        .exec((err, result) => {
            if (err) {
                return res.status(422).json({ error: err })
            }
            res.json({ message: "OK", data: result })
        })
})

router.get('/get-following-requests', requireLogin, (req, res) => {

    User.findById({ '_id': req.user._id })
        .select("id")
        .populate("followingRequests", "_id name picture")
        .exec((err, result) => {
            if (err) {
                return res.status(422).json({ error: err })
            }
            res.json({ message: "OK", data: result })
        })
})

router.get('/get-my-followers-list/', requireLogin, (req, res) => {

    User.findById(req.user._id)
        .select("id")
        .populate("followers", "_id name picture")
        .exec((err, result) => {
            if (err) {
                return res.status(422).json({ error: err })
            }
            res.json({ message: "OK", data: result })
        })
})

router.get('/get-my-following-list/', requireLogin, (req, res) => {

    User.findById(req.user._id)
        .select("id")
        .populate("following", "_id name picture")
        .exec((err, result) => {
            if (err) {
                return res.status(422).json({ error: err })
            }
            res.json({ message: "OK", data: result })
        })
})


router.get('/user/get-user-details/:userId', requireLogin, (req, res) => {
    
    User.findById({ '_id': req.params.userId })
        .select("id name email picture")
        .exec((err, result) => {
            if (err) {
                return res.status(422).json({ error: err })
            }
            res.json({ message: "OK", data: result })
        })
})

//get friends
router.get("/user/friends/:userId", async (req, res) => {
    try {
        const user = await User.findById(req.params.userId);
        const friends = await Promise.all(
        user.following.map((friendId) => {
            return User.findById(friendId);
        })
        );
        let friendList = [];
        friends.map((friend) => {
        const { _id, name, email, picture } = friend;
        friendList.push({ _id, name, email, picture });
        });
        res.json({ message: "OK", data: friendList })
        //res.status(200).json(friendList)
    } catch (err) {
        return res.status(422).json({ error: err })
    }
});


module.exports = router;