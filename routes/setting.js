const express = require('express')
const router = express.Router()

const mongoose = require('mongoose')
const requireLogin = require('../middleware/requireLogin')
const Post = mongoose.model("Post")
const User = mongoose.model("User")
const PrivacyType = mongoose.model("PrivacyType")
const Setting = mongoose.model("Setting")
const ObjectId = require('mongodb').ObjectID;


router.get('/privacy-types', requireLogin, (req, res) => {

    PrivacyType.find()
        .then(types => {
            res.json({ message: "Ok", data: types })
        })
        .catch(err => {
            console.log(err)
        })
})

router.get('/get-settings/:settingsId', requireLogin, (req, res) => {
    Setting.findById({ '_id': req.params.settingsId })
    .populate("postsPrivacy", "type")
    .populate("messagesPrivacy", "type")
    .exec((err, result) => {
        if (err) {
            return res.status(422).json({ error: err })
        }
        res.json({ message: "OK", data: result })
    })
})

router.post('/add-new-settings', requireLogin, (req, res) => {

    const { postsPrivacy, messagesPrivacy, settingsId } = req.body
    const  userId = req.user._id.toString();

    if (settingsId === "" || settingsId === undefined || settingsId === "") {
        const setting = new Setting({
            postsPrivacy,
            messagesPrivacy,
            userId,
        })

        setting.save()
            .then(savedSetting => {
                User.findByIdAndUpdate(userId, {
                    $set: { settingsId: ObjectId(savedSetting.id) }
                }, {
                    new: true    // to send updated new record
                })
                    .exec((err, response) => {

                        if (err) {
                            return res.status(422).json({ error: err })
                        }
                        else {
                            return res.json({ message: "Ok", data: savedSetting })
                        }
                    })



            })
            .catch(err => {
                console.log(err)
                return res.status(422).json({ error: err })
                
            })

    }
    else {
        return res.json({ message: "Ok" })
    }
})




module.exports = router;