const mongoose = require('mongoose')

const PrivacyTypeSchema = new mongoose.Schema(
  {
    type: {
      type: String
    }
  }
);

module.exports = mongoose.model("PrivacyType", PrivacyTypeSchema);
