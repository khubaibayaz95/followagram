const mongoose = require('mongoose')
const {ObjectId} = mongoose.Schema.Types

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    resetToken: {
        type: String
    },
    expireToken: {
        type: Date
    },
    picture: {
        type: String,
        default: "https://res.cloudinary.com/instacloneappudemy/image/upload/v1628676433/userImg_hzdebu.png"
    },
    followers: [{
        type: ObjectId,
        ref: "User"
    }],
    following: [{
        type: ObjectId,
        ref: "User"
    }],
    followersRequests: [{
        type: ObjectId,
        ref: "User"
    }],
    followingRequests: [{
        type: ObjectId,
        ref: "User"
    }],
    settingsId: {
        type: ObjectId,
        ref: "Setting"
    },
})

mongoose.model("User", userSchema);
