const mongoose = require('mongoose')
const {ObjectId} = mongoose.Schema.Types

const MessageSchema = new mongoose.Schema(
  {
    conversationId: {
        type: String,
    },
    sender: {
        type: ObjectId,
        ref: "User"
    },
    text: {
        type: String
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model("Message", MessageSchema);
