const mongoose = require('mongoose')
const { ObjectId } = mongoose.Schema.Types

const SettingSchema = new mongoose.Schema(
  {
    userId: {
      type: ObjectId,
      ref: "User"
    },
    postsPrivacy: {
      type: ObjectId,
      ref: "PrivacyType"
    },
    messagesPrivacy: {
      type: ObjectId,
      ref: "PrivacyType"
    },
  }
);

module.exports = mongoose.model("Setting", SettingSchema);
