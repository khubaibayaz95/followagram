import React, { useEffect, createContext, useContext, useReducer, createRef } from 'react'
import NavBar from "./components/navbar";
import "./App.css"
import { BrowserRouter, Route, Switch, useHistory } from 'react-router-dom'
import Home from './components/screens/Home'
import Profile from './components/screens/Profile'
import Signin from './components/screens/Signin'
import Signup from './components/screens/Signup'
import UserProfile from './components/screens/UserProfile'
import CreatePost from './components/screens/CreatePost'
import FollowedUserPosts from './components/screens/FollowedUserPosts'
import ResetPassword from './components/screens/ResetPassword'
import NewPassword from './components/screens/NewPassword'
import MainTabComponent from './components/screens/Requests/index'
import Loader from './components/screens/Loader'
import { reducer, initialState } from './reducers/userReducer'
import {HideLoader} from '../src/components/Common/SharedFunctions'
import MessageIndex from './components/screens/Messages/MessageIndex';
import ImageSlider from './components/screens/ImageSlideDemo';
import { io } from "socket.io-client"
import Settings from './components/screens/Settings';

export const UserContext = createContext()
export const SocketContext = createContext()
const socket = createRef();

const Routing = () => {
  const history = useHistory();
  const { state, dispatch } = useContext(UserContext)


  useEffect(() => {
    const user = JSON.parse(sessionStorage.getItem('user'))

    if (user) {
      dispatch({ type: "USER", payload: user })
      if(socket && socket.current === null) {
        socket.current = io();
        socket.current.emit("addUserToSocket", user._id)
      }
    }
    else {
      if (!history.location.pathname.startsWith('/reset')) {
        HideLoader(true);
        history.push('/signin')
        if(socket.current && socket.current.connected)
          socket.current.close();
      }
    }
  }, [])

  return (
    <Switch>
      <Route exact path="/">
        <Home />
      </Route>
      <Route exact path="/profile">
        <Profile />
      </Route>
      <Route path="/signin">
        <Signin />
      </Route>
      <Route path="/signup">
        <Signup />
      </Route>
      <Route path="/create">
        <CreatePost />
      </Route>
      <Route path="/profile/:userid">
        <UserProfile />
      </Route>
      <Route path="/myfolloweduserposts">
        <FollowedUserPosts />
      </Route>
      <Route exact path="/reset-password">
        <ResetPassword />
      </Route>
      <Route path="/reset-password/:token">
        <NewPassword />
      </Route>
      <Route path="/requests">
        <MainTabComponent />
      </Route>
      <Route path="/messenger">
        <MessageIndex />
      </Route>
      <Route path="/settings">
        <Settings />
      </Route>
      <Route path="/imagesslider">
        <ImageSlider />
      </Route>
    </Switch>
  )
}

function App() {
  const [state, dispatch] = useReducer(reducer, initialState)
  return (
    <UserContext.Provider value={{ state, dispatch }}>
      <SocketContext.Provider value={socket}>
        <BrowserRouter>
          <Loader />
          <NavBar />
          <Routing />
        </BrowserRouter>
      </SocketContext.Provider>
    </UserContext.Provider>
  );
}

export default App;
