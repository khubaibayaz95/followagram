import React, {useContext, useRef, useState} from 'react'
import {Link, useHistory} from 'react-router-dom'
import {UserContext, SocketContext} from '../App'
import { Modal, ModalBody, ModalHeader, ModalFooter, Button, Row, Card, CardBody, Input, Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink } from "reactstrap";
import {HideLoader} from "../components/Common/SharedFunctions"


const NavBar = () => {
    const history = useHistory()
    const {state, dispatch} = useContext(UserContext)
    const socket = useContext(SocketContext)

    const sideMenu = useRef(null)
    const [search, setSearch] = useState("")
    const [userDetails, setUserDetails] = useState([])
    const [stateVariable, setStateData] = useState({
        modal1: false,
        modalId: 0,
        isOpen: false
    });

    const renderList = () => {
        if(state) {
          return(
            <Navbar color="light" light expand="md">
              <NavbarBrand> <NavLink tag={Link} to="/">Followagram</NavLink></NavbarBrand>
              <NavbarToggler onClick={toggle} />
              <Collapse isOpen={stateVariable.isOpen} navbar>
                <Nav className="ms-auto" navbar>
                  <NavItem>
                    <i data-target="modal1" className="large material-icons modal-trigger nav-search-icon" style={{color: 'black', lineHeight: 'var(--bs-body-line-height)'}} onClick={() => {
                      OpenModal(1)
                  }}>search</i>
                  </NavItem>
                  <NavItem>
                    <NavLink tag={Link} to="/profile">Profile</NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink tag={Link} to="/create">Create Post</NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink tag={Link} to="/requests">Requests</NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink tag={Link} to="/myfolloweduserposts">User Posts</NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink tag={Link} to="/messenger">Messenger</NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink tag={Link} to="/settings">Settings</NavLink>
                  </NavItem>
                  <NavItem>
                    <Button 
                      size='sm'
                      color='primary'
                      className='nav-link'
                      onClick={() => {
                        dispatch({type: "LOGOUT"})
                        sessionStorage.clear()
                        history.push('/signin')
                        if(socket && socket.current) {
                          socket.current.close();
                          socket.current = null;
                        }
                      }}
                    >
                      Logout
                    </Button>
                  </NavItem>
                </Nav>
              </Collapse>
            </Navbar>
          )
        }
        else {
          return(
              <Navbar color="light" light expand="md">
                <NavbarBrand href="/">Followagram</NavbarBrand>
                <NavbarToggler onClick={toggle} />
                <Collapse isOpen={stateVariable.isOpen} navbar>
                  <Nav className="mr-auto" navbar>
                    <NavItem>
                      <NavLink tag={Link} to="/signin">Signin</NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink tag={Link} to="/signup">Signup</NavLink>
                    </NavItem>
                  </Nav>
                </Collapse>
              </Navbar>
          )
          
        }
    }

    const searchUsers = (query) => {
        setSearch(query)

        if(query === '') {
            setUserDetails([])
        }
        if(query !== '') {
            HideLoader(false)
            fetch('/search-users', {
                method: "post",
                headers: {
                    'Content-Type' : 'application/json',
                    'Authorization': 'Bearer ' + sessionStorage.getItem('jwt')
                },
                body: JSON.stringify({
                    query
                })
              })
              .then(res => res.json())
              .then(result => {
                if(result.error == "You must be logged in") {
                  history.push('/signin')
                }
                else {      
                    if(result.message == 'OK') {
                        setUserDetails(result.data)
                    }
                }
                HideLoader(true)
              })
              .catch(err => {
                console.log(err)
                HideLoader(true)
            })
        }
    }

    const OpenModal = (modalId) => {
        setStateData((prevState) => {
            return { ...prevState, modalId: modalId };
        });
    };

    const toggle = (e) => {
        setStateData((prevState) => {
            return { ...prevState, 
              isOpen: !stateVariable.isOpen 
            };
        });
      }

    return (
      <div>

      
        {renderList()}

        <Modal
                isOpen={stateVariable.modalId == 1}
                toggle={(e) => OpenModal(1)}
                className="com-opt-modal text-center"
                backdrop="static"
                size="lg"
            >
                <ModalHeader toggle={(e) => OpenModal(0)}>
                    Search Users
                </ModalHeader>
                <ModalBody>
                    <Row>
                        <Card className="h6">
                            <CardBody>
                                <Input
                                    type="text"
                                    name="SearchUsers"
                                    maxLength={20}
                                    autoComplete="off"
                                    placeholder="Search Users"
                                    className="form-control input-sm"
                                    value={search}
                                    onChange={(e) => {searchUsers(e.target.value)}}
                                />

                            <ul className="collection">
                                {
                                        userDetails.map(item => {
                                            return <li key={item._id} className="collection-item style-none"> 
                                            <Link to={"/profile/" + item._id} onClick={() => {
                                                setSearch('')
                                                setUserDetails([])
                                                OpenModal(0);
                                            }}>
                                                <img className="popup-image" style={{width: '30px', height: '30px', borderRadius: '60px', marginRight: '10px'}} 
                                                    src={item.picture? item.picture : 'loading..'}
                                                />
                                                {item.email}
                                            </Link>
                                        </li>
                                        })
                                }
                                {
                                    userDetails.length == 0 && <li key={1} className="collection-item style-none"> No users found </li>
                                }
                            </ul>

                            </CardBody>
                        </Card>
                    </Row>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" size='sm' onClick={(e) => { OpenModal(0); }}>
                        Close
                    </Button>
                </ModalFooter>
            </Modal>
      </div>
    )
}

export default NavBar