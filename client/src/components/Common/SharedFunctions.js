
import { Modal, ModalBody, ModalHeader, ModalFooter, Button, Container, Row, Col } from "reactstrap";
import React, { forwardRef, useState, useRef, useImperativeHandle, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faExclamationTriangle, faInfoCircle, faCheckCircle, faInfo } from "@fortawesome/free-solid-svg-icons";

export const HideLoader = (ishidded) => {
    document.getElementsByClassName('loaderContainer')[0].style.display = (ishidded == false ? 'block' : 'none');
}


const fetchFileName = (picture) => {
    let name = picture.split("/");

    return name[name.length - 1];
}
export const preparePhotosList = (data) => {
    let d = data ? data.map(d => {
        let obj = {
            src: d.picture,
            altText: "",
            caption: "",
            imageFileName: fetchFileName(d.picture),
            imageFileDescription: d.body,
            id: d._id
        }
        return obj
    }) : []
    return sortPhotoList(d)
}
export const sortPhotoList = (pl) => {
    let arr = [];
    for (let i = 0; i < pl.length; i++) {
        if (pl[i].useInQuotes) {
            arr.push(pl[i]);
            pl.splice(i, 1);
        }
    }
    pl.sort((a, b) => (a.imageFileName.toLowerCase() < b.imageFileName.toLowerCase()) ? -1 : ((b.imageFileName.toLowerCase() > a.imageFileName.toLowerCase()) ? 1 : 0));
    return arr.concat(pl);
}



export const ConfirmModal = forwardRef((props, ref) => {

    const [isOpenConfirmDialog, setIsOpenConfirmDialog] = useState(false)
    const [confirmDialogTitle, setConfirmDialogTitle] = useState("")
    const [confirmDialogMessage, setConfirmDialogMessage] = useState("")

    useImperativeHandle(ref, () => ({

        showConfirmDialog(title, message) {
            setConfirmDialogTitle(title);
            setConfirmDialogMessage(message);
            setIsOpenConfirmDialog(true)
        }

    }));

    const hideConfirmDialog = () => {
        setConfirmDialogTitle("");
        setConfirmDialogMessage("");
        setIsOpenConfirmDialog(false);
    }


    return (
        <div>
            <Modal
                isOpen={isOpenConfirmDialog}
                toggle={props.toggle}
                size="md"
                backdrop="static"
                className="text-center"
                aria-labelledby="contained-modal-title-vcenter"
                centered style={{ minWidth: "600px" }}
            >
                <ModalHeader className="errorMsg" close={<button className="close" onClick={() => { hideConfirmDialog(); }}>×</button>}>
                    <FontAwesomeIcon icon={confirmDialogTitle == "Confirm Action?" || confirmDialogTitle == "Unsaved Changes" || confirmDialogTitle == "Confirm Delete" ? faExclamationTriangle : faInfoCircle} />
                    <h5>{confirmDialogTitle}</h5>
                </ModalHeader>
                <ModalBody style={{ paddingTop: "0px", paddingBottom: "0px" }}>

                    {
                        props.isMessageHTML ? (
                            confirmDialogMessage
                        ) : (
                            <p>{confirmDialogMessage}</p>
                        )
                    }


                </ModalBody>
                <ModalFooter className="unsavedChangesModal">


                    <Button color="primary" type="submit" onClick={() => { props.onSuccess(); hideConfirmDialog(); }} >
                        {
                            props.ConfirmBtnText ? props.ConfirmBtnText : "Yes"
                        }
                    </Button>

                    {(!props.noCancelButton) ? (
                        <Button color="secondary" className="text-dark" onClick={() => { hideConfirmDialog(); if (props.onCancel) { props.onCancel() } }}>
                            {
                                props.CancelBtnText ? props.CancelBtnText : "No, cancel"
                            }
                        </Button>)
                        : ""}




                </ModalFooter>
            </Modal>
        </div>
    )
});


export const AlertModal = forwardRef((props, ref) => {

    const [isOpenAlert, setIsOpenAlert] = useState(false)
    const [alertTitle, setAlertTitle] = useState("")
    const [alertMessage, setAlertMessage] = useState("")
    const [hideBtn, setHideBtn] = useState(props.hideClose == undefined ? false : props.hideClose)
    const [isHtml, setIsHtml] = useState(props.isHtml == undefined ? false : props.isHtml)




    useImperativeHandle(ref, () => ({

        showAlert(title, message) {
            setAlertTitle(title);
            setAlertMessage(message);
            setIsOpenAlert(true);
        }

    }));

    const hideAlert = () => {
        setIsOpenAlert(false);
        setAlertTitle("");
        setAlertMessage("");

    }


    return (
        <div>
            <Modal
                isOpen={isOpenAlert}
                toggle={props.toggle}
                fade={false}
                //onClosed={deleteEnquiryCloseModal}
                className="text-center"
                backdrop="static"
                aria-labelledby="contained-modal-title-vcenter"
                centered style={{ minWidth: "600px" }}
            >
                <ModalHeader className={alertTitle == "Success" ? "sucessMsg" : "errorMsg"} close={hideBtn ? <p onClick={() => { hideAlert(); if (props.onClose) props.onClose() }}></p> : ''}>
                    <FontAwesomeIcon icon={alertTitle == "Success" ? faCheckCircle : faExclamationTriangle} />
                    <h5>{alertTitle}</h5>
                </ModalHeader>
                <ModalBody style={{ paddingTop: "0px", paddingBottom: "0px" }}>
                    {!isHtml && (<p>{alertMessage} </p>)}
                    {isHtml && (<div className='mt-2' style={{ fontSize: "14px", marginBottom: '15px' }} dangerouslySetInnerHTML={{ __html: alertMessage }}></div>)}
                </ModalBody>
                <ModalFooter className="sucessModalFooter" style={{ padding: "0px" }}>
                    <Button color="primary" onClick={() => { hideAlert(); if (props.onClose) props.onClose() }}>OK</Button>
                    {" "}
                </ModalFooter>
            </Modal>
        </div>
    )
});

export const ChangedModal = forwardRef((props, ref) => {

    const [isOpenConfirmDialog, setIsOpenConfirmDialog] = useState(false)

    useImperativeHandle(ref, () => ({

        showConfirmDialog() {
            setIsOpenConfirmDialog(true)
        }

    }));

    const hideConfirmDialog = () => {
        setIsOpenConfirmDialog(false);
    }


    return (
        <div>
            <Modal
                isOpen={isOpenConfirmDialog}
                toggle={props.toggle}
                fade={false}
                className="text-center"
                backdrop="static"
                aria-labelledby="contained-modal-title-vcenter"
                centered style={{ minWidth: "600px" }}
            >
                <ModalHeader className="errorMsg">
                    <FontAwesomeIcon icon={faExclamationTriangle} />
                    <h5 className="font-weight-bold" >Unsaved Changes</h5>
                </ModalHeader>
                <ModalBody style={{ paddingTop: "0px", paddingBottom: "0px" }}>
                    <p>If you have made any changes that are not saved yet, all the changes will be lost. Are you sure you want to continue?</p>
                </ModalBody>
                <ModalFooter className="unsavedChangesModal">
                    <Button type="submit" color="primary" onClick={() => { props.onSuccess(); hideConfirmDialog(); }} >
                        Yes, discard changes
                    </Button>
                    <Button color="secondary" className="text-dark" onClick={() => { hideConfirmDialog(); if (props.onCancel) { props.onCancel() } }}>
                        No, cancel
                    </Button>




                </ModalFooter>
            </Modal>
        </div>
    )
});





export const AlertModalOnCloseCustomFunction = forwardRef((props, ref) => {

    const [isOpenAlert, setIsOpenAlert] = useState(false)
    const [alertTitle, setAlertTitle] = useState("")
    const [alertMessage, setAlertMessage] = useState("")
    const [messageType, setMessageType] = useState('');
    const [hideBtn, setHideBtn] = useState(props.hideClose == undefined ? false : props.hideClose)
    const [isHtml, setIsHtml] = useState(props.isHtml == undefined ? false : props.isHtml)


    const toggleAlert = () => setIsOpenAlert(!isOpenAlert);

    useImperativeHandle(ref, () => ({

        showAlert(title, message, messageType) {
            setAlertTitle(title);
            setAlertMessage(message);
            setIsOpenAlert(true);
            setMessageType(messageType);
        }

    }));

    const hideAlert = () => {
        // setAlertTitle("");
        // setAlertMessage("");
        setIsOpenAlert(false);
    }



    return (
        <div>
            <Modal
                className='text-center'
                isOpen={isOpenAlert}
                toggle={toggleAlert}
                fade={true}
                //onClosed={deleteEnquiryCloseModal}
                //className={props.className}
                backdrop="static"
                aria-labelledby="contained-modal-title-vcenter"
                centered style={{ minWidth: "600px" }}
            >
                <ModalHeader className={alertTitle == "Success" ? "sucessMsg" : "errorMsg"} toggle={() => { hideAlert(); if (props.onClose) props.onClose() }}>
                    <FontAwesomeIcon icon={alertTitle == "Success" ? faCheckCircle : faExclamationTriangle} />
                    <h5>{alertTitle}</h5>
                </ModalHeader>
                <ModalBody style={{ paddingTop: "0px", paddingBottom: "0px" }}>

                    {!isHtml && (<p className='mt-2' style={{ fontSize: "14px", marginBottom: '15px' }}>{alertMessage} </p>)}

                    {isHtml && (<div className='mt-2' style={{ fontSize: "14px", marginBottom: '15px' }} dangerouslySetInnerHTML={{ __html: alertMessage }}></div>)}
                </ModalBody>
                <ModalFooter className="errorModalFooter" style={{ padding: "0px" }}>
                    <Button color="primary" onClick={() => { hideAlert(); if (props.onClose) props.onClose() }}>OK</Button>
                    {" "}
                </ModalFooter>
            </Modal>
        </div>
    )

});