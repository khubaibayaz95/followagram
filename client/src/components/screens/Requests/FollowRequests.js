import React, { useContext, useEffect, useState, useRef } from 'react'
import { useHistory, Link } from 'react-router-dom'
import { UserContext } from '../../../App'
import { HideLoader, AlertModal } from "../../Common/SharedFunctions"
import { Row, Col, Button } from 'reactstrap'

const FollowRequests = () => {

    const history = useHistory();
    const { state, dispatch } = useContext(UserContext)
    const [followersRequestList, setFollowersRequestList] = useState([])

    const refAlertInfo = useRef()
    useEffect(() => {

        let userId = JSON.parse(sessionStorage.getItem('user'))
        userId = userId._id;
        HideLoader(false)
        fetch(`/get-followers-requests/${userId}`, {
            headers: {
                'Authorization': 'Bearer ' + sessionStorage.getItem('jwt')
            }
        })
            .then(res => res.json())
            .then(result => {
                if (result.error == "You must be logged in") {
                    history.push('/signin')
                }
                else {
                    if (result.data) {
                        setFollowersRequestList(result.data.followersRequests)
                    }
                }
                HideLoader(true)
            })
            .catch(err => {
                console.log(err)
                HideLoader(true)
            })
    }, [])

    const rejectFollowRequest = (userId) => {
        HideLoader(false)
        fetch(`/reject-follow-request`, {
            method: 'put',
            headers: {
                'Content-Type': 'Application/json',
                'Authorization': 'Bearer ' + sessionStorage.getItem('jwt')
            },
            body: JSON.stringify({
                followId: userId
            })
        })
            .then(res => res.json())
            .then(data => {
                if (data.error == "You must be logged in") {
                    history.push('/signin')
                }
                else {
                    sessionStorage.setItem('user', JSON.stringify({ ...state, followersRequests: data.result.followersRequests }))
                    dispatch({ type: "UPDATE_FOLLOWERS_REQUESTS", payload: { followersRequests: data.result.followersRequests } })

                    let newData = followersRequestList.filter(item => {
                        return userId !== item._id
                    })
                    setFollowersRequestList(newData)

                    if (data.message == "OK") {
                        refAlertInfo.current.showAlert("Success", "Request deleted successfully.")

                    }
                }
                HideLoader(true)
            })
            .catch(err => {
                console.log(err)
                HideLoader(true)
            })
    }
    const acceptFollowRequest = (userId) => {
        HideLoader(false)
        fetch(`/accept-follow-request`, {
            method: 'put',
            headers: {
                'Content-Type': 'Application/json',
                'Authorization': 'Bearer ' + sessionStorage.getItem('jwt')
            },
            body: JSON.stringify({
                followId: userId
            })
        })
            .then(res => res.json())
            .then(data => {
                if (data.error == "You must be logged in") {
                    history.push('/signin')
                }
                else {
                    sessionStorage.setItem('user', JSON.stringify({ ...state, followers: data.result.followers, followersRequests: data.result.followersRequests }))
                    dispatch({ type: "UPDATE_ACCEPT_FOLLOW_REQUESTS", payload: { followers: data.result.followers, followersRequests: data.result.followersRequests } })

                    let newData = followersRequestList.filter(item => {
                        return userId !== item._id
                    })
                    setFollowersRequestList(newData)

                    if (data.message == "OK") {
                        refAlertInfo.current.showAlert("Success", "Follow request accepted.")
                    }
                }
                HideLoader(true)
            })
            .catch(err => {
                console.log(err)
                HideLoader(true)
            })
    }

    return (
        <div style={{ maxWidth: '60%', margin: '0 auto' }} className="page-width-mobile">

            <Row style={{ margin: '18px 0px', borderBottom: '1px solid grey' }}>
                <Col md="12">
                    <ul className="collection">
                        {
                            followersRequestList.map(item => {
                                return <li key={item._id} className="collection-item requests">

                                    <Col md="10">

                                        <Link to={"/profile/" + item._id}>
                                            <img className="popup-image" style={{ width: '30px', height: '30px', borderRadius: '60px', marginRight: '10px' }}
                                                src={item.picture ? item.picture : 'loading..'}
                                            />
                                            {item.name}
                                        </Link>
                                    </Col>
                                    <Col md="2">
                                        <Button
                                            color='primary'
                                            size='sm'
                                            onClick={() => acceptFollowRequest(item._id)}
                                        >
                                            Accept
                                        </Button>
                                        {" "}
                                        <Button
                                            color='secondary'
                                            size='sm'
                                            onClick={() => rejectFollowRequest(item._id)}
                                        >
                                            Delete
                                        </Button>
                                    </Col>

                                </li>
                            })
                        }
                        {
                            followersRequestList.length == 0 && <li key={1} className="collection-item style-none"> No New Request</li>
                        }
                    </ul>
                </Col>

            </Row>

            <AlertModal ref={refAlertInfo} />
        </div>
    )
}

export default FollowRequests