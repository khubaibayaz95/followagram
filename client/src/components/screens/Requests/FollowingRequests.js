import React, { useContext, useEffect, useState, useRef } from 'react'
import { useHistory, Link } from 'react-router-dom'
import { UserContext } from '../../../App'
import { HideLoader, AlertModal } from "../../Common/SharedFunctions"
import { Row, Col, Button } from 'reactstrap'

const FollowingRequests = () => {

    const history = useHistory();
    const { state, dispatch } = useContext(UserContext)
    const [followingRequestList, setFollowingRequestList] = useState([])
    const refAlertInfo = useRef()

    useEffect(() => {
        HideLoader(false)
        fetch('/get-following-requests', {
            headers: {
                'Authorization': 'Bearer ' + sessionStorage.getItem('jwt')
            }
        })
            .then(res => res.json())
            .then(result => {
                if (result.error == "You must be logged in") {
                    history.push('/signin')
                }
                else {
                    if (result.data) {
                        setFollowingRequestList(result.data.followingRequests)
                    }
                }
                HideLoader(true)
            })
            .catch(err => {
                console.log(err)
                HideLoader(true)
            })
    }, [])

    const cancelFollowingRequest = (userId) => {
        HideLoader(false)
        fetch(`/cancel-follow-request`, {
            method: 'put',
            headers: {
                'Content-Type': 'Application/json',
                'Authorization': 'Bearer ' + sessionStorage.getItem('jwt')
            },
            body: JSON.stringify({
                followId: userId
            })
        })
            .then(res => res.json())
            .then(data => {
                if (data.error == "You must be logged in") {
                    history.push('/signin')
                }
                else {
                    sessionStorage.setItem('user', JSON.stringify({ ...state, followingRequests: data.result.followingRequests }))
                    dispatch({ type: "UPDATE_FOLLOWING_REQUESTS", payload: { followingRequests: data.result.followingRequests } })


                    let newData = followingRequestList.filter(item => {
                        return userId !== item._id
                    })
                    setFollowingRequestList(newData)

                    if (data.message == "OK") {
                        refAlertInfo.current.showAlert("Success", "Request cancelled successfully.")
                    }
                }
                HideLoader(true)
            })
            .catch(err => {
                console.log(err)
                HideLoader(true)
            })
    }

    return (
        <div style={{ maxWidth: '60%', margin: '0 auto' }} className="page-width-mobile">
            <Row style={{ margin: '18px 0px', borderBottom: '1px solid grey' }}>
                <Col md="12">
                    <ul className="collection">
                        {
                            followingRequestList.map(item => {
                                return <li key={item._id} className="collection-item requests">
                                    <Col md="11">
                                        <Link to={"/profile/" + item._id}>
                                            <img className="popup-image" style={{ width: '30px', height: '30px', borderRadius: '60px', marginRight: '10px' }}
                                                src={item.picture ? item.picture : 'loading..'}
                                            />
                                            {item.name}
                                        </Link>
                                    </Col>

                                    <Col md="1">
                                        <Button
                                            color='secondary'
                                            size='sm'
                                            onClick={() => cancelFollowingRequest(item._id)}
                                        >
                                            Cancel
                                        </Button>
                                    </Col>
                                </li>
                            })
                        }
                        {
                            followingRequestList.length == 0 && <li key={1} className="collection-item style-none"> No Request Sent </li>
                        }
                    </ul>
                </Col>
            </Row>
            <AlertModal ref={refAlertInfo} />
        </div>
    )
}

export default FollowingRequests