import Tabs from "./Tabs";
import FollowRequests from './FollowRequests'
import FollowingRequests from './FollowingRequests'

const MainTabComponent = () => {

    return (
        <div style={{marginTop: '2%'}}>
            <Tabs>
                <div label="Followers">
                    <FollowRequests />
                </div>  
                <div label="Following">
                    <FollowingRequests />
                </div>
            </Tabs>
        </div>
    )
}

export default MainTabComponent;