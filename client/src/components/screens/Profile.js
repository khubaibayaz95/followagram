import React, { useContext, useEffect, useState, useRef } from 'react'
import { useHistory, Link } from 'react-router-dom'
import { UserContext } from '../../App'
import Lightbox from 'react-image-lightbox'
import 'react-image-lightbox/style.css';
import { HideLoader, AlertModal, preparePhotosList } from "../../components/Common/SharedFunctions"
import {
    Carousel,
    Card,
    CardBody,
    CarouselItem,
    CarouselControl,
    CarouselIndicators,
    Modal, ModalBody,
    ModalFooter, ModalHeader,
    Button,
    Row,
    Col
} from "reactstrap";

const Profile = () => {

    const history = useHistory();
    const { state, dispatch } = useContext(UserContext)
    const [mypics, setMypics] = useState([])
    const [image, setImage] = useState("")
    const [followersListModal, setFollowersListModal] = useState([])
    const [followingListModal, setFollowingListModal] = useState([])
    const [activeIndex, setActiveIndex] = useState(0);
    const [animating, setAnimating] = useState(false);
    const refAlertInfo = useRef()
    const [stateVariable, setStateData] = useState({
        modal1: false,
        modalId: 0,
    });



    // const [photoIndex, setPhotoIndex ] = useState(0)
    const [imagesPost, setImagesPost] = useState([])
    // const [isOpen, setIsOpen ] = useState(false)
    const [savedSummaryModal, setSavedSummaryModal] = useState(false);


    useEffect(() => {
        HideLoader(false)
        fetch('/mypost', {
            headers: {
                'Authorization': 'Bearer ' + sessionStorage.getItem('jwt')
            }
        })
            .then(res => res.json())
            .then(result => {
                if (result.error == "You must be logged in") {
                    history.push('/signin')
                }
                else {

                    setMypics(result.data)
                    setImagesPost(preparePhotosList(result.data))
                    if (result.userDetails) {
                        dispatch({ type: "UPDATE", payload: { following: result.userDetails.following, followers: result.userDetails.followers, picture: result.userDetails.picture } })
                        sessionStorage.setItem('user', JSON.stringify({ ...state, following: result.userDetails.following, followers: result.userDetails.followers, picture: result.userDetails.picture }))
                    }
                }

                HideLoader(true)
            })
            .catch(err => {
                console.log(err)
                HideLoader(true)
            })
    }, [])

    useEffect(() => {
        if (image) {
            let data = new FormData()

            data.append("file", image)
            data.append("upload_preset", "InstaClone")     // name of preset in cloudinary
            data.append("cloud_name", "AS")

            HideLoader(false)
            fetch("https://api.cloudinary.com/v1_1/instacloneappudemy/image/upload", {
                method: "post",
                body: data
            })
                .then(res => res.json())
                .then(data => {
                    updateProfilePicOnDb(data.url)
                    HideLoader(true)
                })
                .catch(err => {
                    console.log(err)
                    HideLoader(true)
                })
        }

    }, [image])

    const onExiting = () => {
        setAnimating(true);
    }

    const onExited = () => {
        setAnimating(false);
    }

    useEffect(() => {
        if (imagesPost) {
            slides = imagesPost.map((item, index) => {
                return (
                    <CarouselItem tag="div" onExiting={onExiting} onExited={onExited} key={item.id}>
                        <img style={{ width: "400px", height: "340px" }} src={item.src} />
                        <div className="text-center">
                            <p className="pb-0 mb-0 font-weight-bold">{item.imageFileName}</p>
                        </div>
                    </CarouselItem>
                );
            });
        }
    }, [imagesPost]);


    const toggleFollowersModal = () => {

        if (state && !state.followers.length > 0) {
            refAlertInfo.current.showAlert("Warning", "You don't have any follower.")
            return;
        }
        HideLoader(false)
        fetch('/get-my-followers-list', {
            headers: {
                'Authorization': 'Bearer ' + sessionStorage.getItem('jwt')
            }
        })
            .then(res => res.json())
            .then(result => {
                if (result.error == "You must be logged in") {
                    history.push('/signin')
                }
                else {
                    setFollowersListModal(result.data.followers)
                    OpenModal(1)
                }
                HideLoader(true)
            })
            .catch(err => {
                console.log(err)
                HideLoader(true)
            })
    }

    const toggleFollowingModal = () => {
        if (state && !state.following.length > 0) {
            refAlertInfo.current.showAlert("Warning", "You're not following anyone.")
            return;
        }
        HideLoader(false)
        fetch('/get-my-following-list', {
            headers: {
                'Authorization': 'Bearer ' + sessionStorage.getItem('jwt')
            }
        })
            .then(res => res.json())
            .then(result => {
                if (result.error == "You must be logged in") {
                    history.push('/signin')
                }
                else {
                    setFollowingListModal(result.data.following)
                    OpenModal(2)
                }
                HideLoader(true)
            })
            .catch(err => {
                console.log(err)
                HideLoader(true)
            })
    }

    const updateProfilePicOnDb = (imageUrl) => {
        HideLoader(false)
        fetch('/updateprofilepicture', {
            method: 'put',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + sessionStorage.getItem('jwt')
            },
            body: JSON.stringify({
                picture: imageUrl
            })
        })
            .then(res => res.json())
            .then(result => {

                sessionStorage.setItem('user', JSON.stringify({ ...state, picture: result.data.picture }))
                dispatch({ type: "UPDATEPIC", payload: result.data.picture })
                HideLoader(true)
                refAlertInfo.current.showAlert("Success", "Picture Updated successfully.")

            })
            .catch(err => {
                console.log(err)
                HideLoader(true)
            })

    }

    const updatePhoto = (file) => {
        setImage(file)
    }



    const next = () => {
        if (animating) return;
        let nextIndex;
        let items = imagesPost
        if (activeIndex === 1 && items && items.length === 2) {
            nextIndex = 2;
            setActiveIndex(nextIndex);
            setActiveIndex(0);
        }
        else {
            nextIndex =
                activeIndex === items.length - 1
                    ? 0
                    : activeIndex + 1;
            setActiveIndex(nextIndex);
        }

    }

    const previous = () => {

        if (animating) return;
        let items = imagesPost;
        const nextIndex =
            activeIndex === 0
                ? items.length - 1
                : activeIndex - 1;
        setActiveIndex(nextIndex);
    }

    const goToIndex = (newIndex) => {
        if (animating) return;
        setActiveIndex(newIndex);
    }

    let slides = imagesPost ? imagesPost.map((item, index) => {
        return (
            <CarouselItem tag="div" onExiting={onExiting} onExited={onExited} key={item.id}>
                <img style={{ width: "400px", height: "340px" }} src={item.src} />
                <div className="text-center">
                    <p className="pb-0 mb-0 font-weight-bold">{item.title}</p>
                </div>
            </CarouselItem>
        );
    }) : []


    const openPopup = () => {
        setSavedSummaryModal(true);
    }

    const savedSummaryToggle = () => {
        setSavedSummaryModal(!savedSummaryModal);
    };


    const closeSavedSummaryModal = () => {
        setSavedSummaryModal(false);
    };




    const OpenModal = (modalId) => {
        setStateData((prevState) => {
            return { ...prevState, modalId: modalId };
        });
    };



    const ShowGalleryPictures = () => {
        let columns = [];
        mypics.forEach((item, idx) => {
            columns.push(
                <Col md="3">
                    <img className='gallery-pictures' key={item._id} alt={item.title} src={item.picture} />
                </Col>
            )
            if ((idx+1)%4===0) {columns.push(<Row className='m-t'></Row>)}
        })
        return (
            <Row>
                {columns}
            </Row>
        )
    }

    return (
        <div style={{ maxWidth: '60%', margin: '0 auto' }}>


            <Row style={{ margin: '18px 0px', borderBottom: '1px solid grey' }}>

                <Col md="4">
                    <div>
                        <img className="profile-picture" style={{ width: '160px', height: '160px', borderRadius: '80px' }}
                            src={state ? state.picture : 'loading..'}
                        />
                    </div>



                    <div style={{ textAlign: 'center', marginTop: '15px', marginBottom: '15px' }}>
                        <div className="file-field input-field">
                            <div className="btn custom-btn waves-effect waves-light #64b5f6 blue darken-1">
                                <span>Update Photo</span>
                                <input type="file" onChange={(e) => updatePhoto(e.target.files[0])} />
                            </div>
                            <div style={{ display: 'none' }} className="file-path-wrapper">
                                <input id="imagePath" className="file-path validate" type="text" />
                            </div>

                            {/* <div className="row">
                                <button onClick={openPopup} className="btn custom-btn waves-effect waves-light #64b5f6 blue darken-1">
                                    Open Popup
                                </button>
                            </div> */}
                        </div>
                    </div>


                </Col>

                <Col md="8">
                    <Row style={{ textAlign: 'center' }}>
                        <h5>{state ? state.name : "loading"}</h5>
                        <h6>{state ? state.email : "loading"}</h6>
                    </Row>

                    <Row className='profile-summary'>
                        <div className="col-sm-3 mlr tac">
                            <p><strong>{mypics.length}</strong> Posts</p>
                        </div>
                        <div className="col-sm-3 mlr tac">
                            <p onClick={toggleFollowersModal}><strong>{state ? state.followers.length : "0"}</strong> Followers</p>
                        </div>
                        <div className="col-sm-3 mlr tac">
                            <p onClick={toggleFollowingModal}><strong>{state ? state.following.length : "0"}</strong> Following</p>
                        </div>
                    </Row>

                    {/* <div>
                        <Carousel className="bg-secondary" interval={ false } activeIndex={activeIndex} next={next} previous={previous}>
                        <CarouselIndicators items={imagesPost} activeIndex={activeIndex} onClickHandler={goToIndex}/>
                            {slides}
                        <CarouselControl direction="prev" directionText="Previous" onClickHandler={previous}/>
                        <CarouselControl direction="next" directionText="Next" onClickHandler={next}/>
                        </Carousel>
                    </div> */}
                </Col>
            </Row>

            {ShowGalleryPictures()}
            {/* {isOpen && ( 
                <Lightbox
                    mainSrc={imagesPost[photoIndex].picture}
                    nextSrc={imagesPost[(photoIndex + 1) % imagesPost.length].picture}
                    prevSrc={imagesPost[(photoIndex + imagesPost.length - 1) % imagesPost.length].picture}
                    onCloseRequest={() => setIsOpen(false)}
                    onMovePrevRequest={() =>
                        setPhotoIndex((photoIndex + imagesPost.length - 1) % imagesPost.length)
                    }
                    onMoveNextRequest={() =>
                        setPhotoIndex((photoIndex + 1) % imagesPost.length)
                    }
                    imageTitle={imagesPost[photoIndex].body}
                    clickOutsideToClose={false}
                    closeLabel='Close Images'
                />
            )} */}

            <Modal
                isOpen={stateVariable.modalId == 1}
                toggle={(e) => OpenModal(1)}
                className="com-opt-modal text-center"
                backdrop="static"
            >
                <ModalHeader toggle={(e) => OpenModal(0)}>
                    Followers List
                </ModalHeader>
                <ModalBody>
                    <Row>
                        <Card className="h6">
                            <CardBody>
                                <ul className="collection">
                                    {
                                        followersListModal.map(item => {
                                            return <li key={item._id} className="collection-item style-none">
                                                <Link to={"/profile/" + item._id}>
                                                    <img className="popup-image" style={{ width: '30px', height: '30px', borderRadius: '60px', marginRight: '10px' }}
                                                        src={item.picture ? item.picture : 'loading..'}
                                                    />
                                                    {item.name}
                                                </Link>
                                            </li>
                                        })
                                    }
                                </ul>
                            </CardBody>
                        </Card>
                    </Row>
                </ModalBody>
                <ModalFooter>
                    <Button size='sm' color="primary" onClick={(e) => { OpenModal(0); setFollowersListModal([]) }}>
                        Close
                    </Button>
                </ModalFooter>
            </Modal>



            <Modal
                isOpen={stateVariable.modalId == 2}
                toggle={(e) => OpenModal(2)}
                className="com-opt-modal text-center"
                backdrop="static"
            >
                <ModalHeader toggle={(e) => OpenModal(0)}>
                    Following List
                </ModalHeader>
                <ModalBody>
                    <Row>
                        <Card className="h6">
                            <CardBody>
                                <ul className="collection">
                                    {
                                        followingListModal.map(item => {
                                            return <li key={item._id} className="collection-item style-none">
                                                <Link to={"/profile/" + item._id}>
                                                    <img className="popup-image" style={{ width: '30px', height: '30px', borderRadius: '60px', marginRight: '10px' }}
                                                        src={item.picture ? item.picture : 'loading..'}
                                                    />
                                                    {item.name}
                                                </Link>
                                            </li>
                                        })
                                    }
                                </ul>
                            </CardBody>
                        </Card>
                    </Row>
                </ModalBody>
                <ModalFooter>
                    <Button size='sm' color="primary" onClick={(e) => { OpenModal(0); setFollowingListModal([]) }}>
                        Close
                    </Button>
                </ModalFooter>
            </Modal>

            <AlertModal ref={refAlertInfo} />
        </div>


    )
}

export default Profile