import React, { useState, useEffect, useRef } from "react"
import { useHistory } from "react-router-dom"
import { HideLoader, AlertModal } from "../../components/Common/SharedFunctions"
import { Row, Button, Col, Input, Card, CardBody } from "reactstrap";

const CreatePost = () => {

    const history = useHistory()
    const [title, setTitle] = useState("")
    const [body, setBody] = useState("")
    const [image, setImage] = useState("")
    const [url, setUrl] = useState("")
    const refAlertInfo = useRef()


    useEffect(() => {
        if (url) {
            HideLoader(false)
            fetch('/createpost', {
                method: "post",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + sessionStorage.getItem('jwt')
                },
                body: JSON.stringify({
                    title,
                    body,
                    picture: url
                })
            })
                .then(res => res.json())
                .then(data => {
                    if (data.error) {
                        refAlertInfo.current.showAlert("Invalid", data.error)
                    }
                    else {
                        refAlertInfo.current.showAlert("Success", 'Post created successfully')
                        history.push('/profile')
                    }
                    HideLoader(true)
                })
                .catch(err => {
                    HideLoader(true)

                    console.log(err)
                })
        }
    }, [url])

    const postDetails = async () => {
        let data = new FormData()
        data.append("file", image)
        data.append("upload_preset", "InstaClone")     // name of preset in cloudinary
        data.append("cloud_name", "AS")

        await fetch("https://api.cloudinary.com/v1_1/instacloneappudemy/image/upload", {
            method: "post",
            body: data
        })
            .then(res => res.json())
            .then(data => {
                setUrl(data.url)
            })
            .catch(err => {
                console.log(err)
            })
    }


    return (
        <div className="tac">
            <Card
                style={{ fontSize: "14px", maxWidth: '50%', margin: '10px' }}
                className="mt-5 mx-auto"
                id="myNewForm"
            >
                <CardBody
                    className="top-border"
                    style={{
                        paddingTop: "0.6rem",
                        paddingBottom: "0.6rem",
                        paddingLeft: "1rem",
                        paddingRight: "1rem",
                    }}
                >

                    <Row style={{ marginBottom: "5px" }}>
                        <Col lg="3"></Col>
                        <Col lg="6">
                            <Input
                                type="text"
                                placeholder="Title"
                                name="title"
                                maxLength={50}
                                autoComplete="off"
                                value={title}
                                onChange={(e) => { setTitle(e.target.value) }}
                            />
                        </Col>
                        <Col lg="3"></Col>
                    </Row>
                    <Row style={{ marginBottom: "5px" }}>
                        <Col lg="3"></Col>
                        <Col lg="6">
                            <Input
                                type="text"
                                placeholder="Body"
                                name="body"
                                maxLength={50}
                                autoComplete="off"
                                value={body}
                                onChange={(e) => { setBody(e.target.value) }}
                            />
                        </Col>
                        <Col lg="3"></Col>
                    </Row>

                    <Row>
                        <Col sm="12">
                            <div className="file-field input-field">
                                <div>
                                    <input type="file" onChange={(e) => setImage(e.target.files[0])} />
                                </div>
                                <div style={{ display: 'none' }}>
                                    <input className="file-path validate" type="text" />
                                </div>
                            </div>
                        </Col>
                    </Row>

                    <Row>
                        <Col sm="12">
                            <Button
                                size='sm'
                                color='primary'
                                onClick={() => postDetails()}
                            >
                                Create Post
                            </Button>
                        </Col>
                    </Row>

                    <AlertModal ref={refAlertInfo} />
                </CardBody>
            </Card>
        </div>
    );
}

export default CreatePost