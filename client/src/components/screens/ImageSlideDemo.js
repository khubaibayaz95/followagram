import React, { useContext, useEffect, useState, useRef } from 'react'
import { useHistory, Link } from 'react-router-dom'
import { UserContext } from '../../App'
import Lightbox from 'react-image-lightbox'
import 'react-image-lightbox/style.css';

const ImageSlider = () => {

    const history = useHistory();
    const { state, dispatch } = useContext(UserContext)
    const { imageState, setImageState } = useContext({
            imgs: ["/img1.jpg", "/img2.jpg", "/img3.jpg", "/img4.jpg"],
            currentIndex: 0,
            movement: 0,
        })
    
    const IMG_WIDTH = 700;
    const IMG_HEIGHT = 400;

    useEffect(() => {

       
    }, [])

    
    const handleWheel = (e) => {
        handleMovement(e.deltaX)
    }

    const handleMovement = (delta) => {

        setImageState((prevState) => {

            const maxLength = prevState.imgs.length - 1;
            let nextMovement = prevState.movement + delta;
      
            if (nextMovement < 0) {
                nextMovement = 0;
            }
        
            if (nextMovement > maxLength * IMG_WIDTH) {
                nextMovement = maxLength * IMG_WIDTH;
            }

            return {
                ...prevState,
                movement: nextMovement,
            }
        })


    }
    

    return (
        <div style={{ maxWidth: '60%', margin: '0 auto' }}>
           <div className="App">
                <div
                className="main"
                style={{
                    width: `${IMG_WIDTH}px`,
                    height: `${IMG_HEIGHT}px`,
                }}
                onWheel={(e) => handleWheel(e)}
                >
                <div className="swiper">
                    {imageState.imgs.map((src) => {
                        return <img className='slider-img' key={src} src={src} width="100%" height="100%" />;
                    })}
                </div>
                </div>
            </div>
        </div>

        
    )
}

export default ImageSlider