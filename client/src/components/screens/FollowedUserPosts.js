import React, { useState, useEffect, useContext, useRef } from 'react'
import { useHistory } from 'react-router'
import { UserContext } from '../../App'
import { Link } from 'react-router-dom'
import { Card, CardBody, Col, Row, Input } from 'reactstrap'
import { HideLoader, AlertModal } from "../../components/Common/SharedFunctions"

const FollowedUserPosts = () => {

  const { state, dispatch } = useContext(UserContext)
  const [data, setData] = useState([])
  const history = useHistory()
  const refAlertInfo = useRef()

  useEffect(() => {
    HideLoader(false)
    fetch('/getfolloweduserpost', {
      headers: {
        'Authorization': 'Bearer ' + sessionStorage.getItem('jwt'),
        'Content-Type': 'application/json'
      }
    })
      .then(res => res.json())
      .then(result => {
        if (result.error == "You must be logged in") {
          history.push('/signin')
        }
        else {
          setData(result.data)
        }

        HideLoader(true)
      })
      .catch(err => {
        console.log(err)
        HideLoader(true)
      })
  }, [])

  const likePost = (id) => {
    HideLoader(false)
    fetch('/like', {
      method: "put",
      headers: {
        'Authorization': 'Bearer ' + sessionStorage.getItem('jwt'),
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        postId: id
      })
    })
      .then(res => res.json())
      .then(result => {
        if (result.error == "You must be logged in") {
          history.push('/signin')
        }
        else {
          const newData = data.map(item => {
            if (item._id == result.data._id) {
              return result.data
            }
            else {
              return item
            }
          })
          setData(newData)
        }
        HideLoader(true)
      })
      .catch(err => {
        console.log(err)
        HideLoader(true)
      })
  }

  const unlikePost = (id) => {
    HideLoader(false)

    fetch('/unlike', {
      method: "put",
      headers: {
        'Authorization': 'Bearer ' + sessionStorage.getItem('jwt'),
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        postId: id
      })
    })
      .then(res => res.json())
      .then(result => {
        if (result.error == "You must be logged in") {
          history.push('/signin')
        }
        else {
          const newData = data.map(item => {
            if (item._id == result.data._id) {
              return result.data
            }
            else {
              return item
            }
          })
          setData(newData)
        }
        HideLoader(true)
      })
      .catch(err => {
        HideLoader(true)
        console.log(err)
      })
  }

  const makeComment = (text, postId) => {
    HideLoader(false)
    fetch('/comment', {
      method: "put",
      headers: {
        'Authorization': 'Bearer ' + sessionStorage.getItem('jwt'),
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        postId,
        text
      })
    })
      .then(res => res.json())
      .then(result => {
        if (result.error == "You must be logged in") {
          history.push('/signin')
        }
        else {
          const newData = data.map(item => {
            if (item._id == result.data._id) {
              return result.data
            }
            else {
              return item
            }
          })
          setData(newData)
          let id = postId.toString();
          document.getElementById(id).value = '';
        }
        HideLoader(true)
      })
      .catch(err => {
        HideLoader(true)
        console.log(err)
      })
  }

  const deletePost = (postId) => {
    HideLoader(false)
    fetch(`/deletepost/${postId}`, {
      method: "delete",
      headers: {
        'Authorization': 'Bearer ' + sessionStorage.getItem('jwt')
      }
    })
      .then(res => res.json())
      .then(result => {
        if (result.error == "You must be logged in") {
          history.push('/signin')
        }
        else {
          let newData = data.filter(item => {
            return item._id !== result.data._id
          })
          setData(newData)
          refAlertInfo.current.showAlert("Success", result.message)
        }
        HideLoader(true)

      })
      .catch(err => {
        HideLoader(true)
        console.log(err)
      })
  }

  const likeComment = (postId, commentId) => {
    HideLoader(false)
    fetch('/like-comment', {
      method: "put",
      headers: {
        'Authorization': 'Bearer ' + sessionStorage.getItem('jwt'),
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        postId,
        commentId
      })
    })
      .then(res => res.json())
      .then(result => {
        if (result.error == "You must be logged in") {
          history.push('/signin')
        }
        else {
          console.log(result)
          const newData = data.map(item => {
            if (item._id == result.data._id) {
              return result.data
            }
            else {
              return item
            }
          })
          setData(newData)
        }

        HideLoader(true)
      })
      .catch(err => {
        console.log(err)
        HideLoader(true)
      })
  }

  const unlikeComment = (postId, commentId) => {
    HideLoader(false)
    fetch('/unlike-comment', {
      method: "put",
      headers: {
        'Authorization': 'Bearer ' + sessionStorage.getItem('jwt'),
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        postId,
        commentId
      })
    })
      .then(res => res.json())
      .then(result => {
        if (result.error == "You must be logged in") {
          history.push('/signin')
        }
        else {
          console.log(result)
          const newData = data.map(item => {
            if (item._id == result.data._id) {
              return result.data
            }
            else {
              return item
            }
          })
          setData(newData)
        }
        HideLoader(true)
      })
      .catch(err => {
        console.log(err)
        HideLoader(true)
      })
  }

  const deleteComment = (postId, commentId) => {
    HideLoader(false)
    fetch('/delete-comment', {
      method: "put",
      headers: {
        'Authorization': 'Bearer ' + sessionStorage.getItem('jwt'),
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        postId,
        commentId
      })
    })
      .then(res => res.json())
      .then(result => {
        if (result.error == "You must be logged in") {
          history.push('/signin')
        }
        else {
          const newData = data.map(item => {
            if (item._id == result.data._id) {
              return result.data
            }
            else {
              return item
            }
          })
          setData(newData)
        }
        HideLoader(true)
      })
      .catch(err => {
        console.log(err)
        HideLoader(true)
      })
  }


  return (
    <div className="home">
      {
        data.map(item => {
          return (

            <Card key={item._id} className="home-card">
              <CardBody>

                <Row>
                  <h5>
                    <div className="home-card-header">
                      <img style={{ width: '40px', height: '40px', borderRadius: '80px' }}
                        src={item.postedBy.picture ? item.postedBy.picture : 'loading..'}
                      />
                      <Link to={item.postedBy._id !== state._id ? "/profile/" + item.postedBy._id : "/profile"}> {item.postedBy.name} </Link>
                      {item.postedBy._id === state._id &&
                        <i className="material-icons" style={{ float: 'right' }}
                          onClick={() => deletePost(item._id)}
                        >delete</i>
                      }
                    </div>
                  </h5>
                </Row>

                <div style={{ display: 'table' }} className="card-image m-0-auto">
                  <img style={{ minHeight: '400px' }} src={item.picture} />
                </div>


                {item.likes.some(d => d._id === state._id) ?
                  <i className="material-icons"
                    style={{ color: 'red' }}
                    onClick={() => unlikePost(item._id)}
                  >favorite</i>
                  :
                  <i className="material-icons"
                    style={{ color: 'black' }}
                    onClick={() => likePost(item._id)}
                  >favorite</i>
                }

                <h6>{item.likes.length} likes</h6>
                <h6>{item.title}</h6>
                <p>{item.body}</p>
                {
                  item.comments.map(record => {
                    return (
                      <p key={record._id} style={{ marginTop: '10px' }}>

                        <img style={{ width: '30px', height: '30px', borderRadius: '80px', marginRight: '10px' }} src={record.postedBy.picture}></img>
                        <Link to={record.postedBy._id !== state._id ? "/profile/" + record.postedBy._id : "/profile"}>
                          <span className="open-profile-name" style={{ fontWeight: '500' }}>{record.postedBy.name} </span>
                        </Link>
                        {record.text}

                        {record.postedBy._id === state._id &&
                          <i className="material-icons" style={{ float: 'right', fontSize: '18px' }}
                            onClick={() => deleteComment(item._id, record._id)}
                          >delete</i>
                        }

                        {record.commentLikedBy.includes(state._id) ? <i className="material-icons"
                          style={{ color: 'red', float: 'right', fontSize: '18px' }}
                          onClick={() => unlikeComment(item._id, record._id)}
                        >favorite</i> :
                          <i className="material-icons"
                            style={{ color: 'black', float: 'right', fontSize: '18px' }}
                            onClick={() => likeComment(item._id, record._id)}
                          >favorite</i>
                        }
                      </p>
                    )
                  })
                }
                <form onSubmit={(e) => {
                  e.preventDefault()
                  makeComment(e.target[0].value, item._id)
                }}>
                  <Input
                    type="text"
                    placeholder="Add comment"
                    autoComplete="off"
                    id={item._id}
                  />


                </form>
              </CardBody>
            </Card>






          )
        })
      }

      <AlertModal ref={refAlertInfo} />
    </div >
  )
}

export default FollowedUserPosts