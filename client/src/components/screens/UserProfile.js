import React, { useContext, useEffect, useRef, useState } from 'react'
import { Link, useHistory, useParams } from 'react-router-dom'
import { UserContext } from '../../App'
import { HideLoader, AlertModal } from "../../components/Common/SharedFunctions"
import {
    Card,
    CardBody,
    Modal, ModalBody,
    ModalFooter, ModalHeader,
    Button,
    Row,
    Col
} from "reactstrap";

const UserProfile = () => {

    const history = useHistory();
    const { state, dispatch } = useContext(UserContext)
    const [userProfile, setProfile] = useState(null)
    const { userid } = useParams()
    const [followersListModal, setFollowersListModal] = useState([])
    const [followingListModal, setFollowingListModal] = useState([])
    const [stateVariable, setStateData] = useState({
        modal1: false,
        modalId: 0,
    });
    const refAlertInfo = useRef()

    useEffect(() => {
        // fetch(`/user/${userid}`, {
        //     headers: {
        //         'Authorization': 'Bearer ' + sessionStorage.getItem('jwt')
        //     }
        // })
        // .then(res => res.json())
        // .then(result =>{
        //     if(result.error == "You must be logged in") {
        //         history.push('/signin')
        //     }
        //     else {
        //         setProfile(result)
        //     }
        // })
        // .catch(err => {
        //     console.log(err)
        // })
    }, [])

    useEffect(() => {

        if (userid) {
            HideLoader(false)
            fetch(`/user/${userid}`, {
                headers: {
                    'Authorization': 'Bearer ' + sessionStorage.getItem('jwt')
                }
            })
                .then(res => res.json())
                .then(result => {
                    if (result.error == "You must be logged in") {
                        history.push('/signin')
                    }
                    else {
                        setProfile(result)
                    }
                    HideLoader(true)
                })
                .catch(err => {
                    console.log(err)
                    HideLoader(true)
                })
        }
    }, [userid])

    const sendFollowRequest = (userId) => {
        HideLoader(false)
        fetch(`/send-follow-request`, {
            method: 'put',
            headers: {
                'Content-Type': 'Application/json',
                'Authorization': 'Bearer ' + sessionStorage.getItem('jwt')
            },
            body: JSON.stringify({
                followId: userid
            })
        })
            .then(res => res.json())
            .then(data => {
                if (data.error == "You must be logged in") {
                    history.push('/signin')
                }
                else {

                    if (data.result.followingRequests.includes(userid)) {
                        sessionStorage.setItem('user', JSON.stringify({ ...state, followingRequests: data.result.followingRequests }))
                        dispatch({ type: "UPDATE_FOLLOWING_REQUESTS", payload: { followingRequests: data.result.followingRequests } })
                    }
                    var property = { ...userProfile }
                    property.user.followersRequests = data.results.followersRequests;
                    setProfile(property)

                    if (data.message == "OK") {
                        refAlertInfo.current.showAlert("Success", "Request sent successfully.")
                    }
                }
                HideLoader(true)
            })
            .catch(err => {
                console.log(err)
                HideLoader(true)
            })
    }

    const cancelFollowRequest = (userId) => {
        HideLoader(false)
        fetch(`/cancel-follow-request`, {
            method: 'put',
            headers: {
                'Content-Type': 'Application/json',
                'Authorization': 'Bearer ' + sessionStorage.getItem('jwt')
            },
            body: JSON.stringify({
                followId: userid
            })
        })
            .then(res => res.json())
            .then(data => {
                if (data.error == "You must be logged in") {
                    history.push('/signin')
                }
                else {

                    sessionStorage.setItem('user', JSON.stringify({ ...state, followingRequests: data.result.followingRequests }))
                    dispatch({ type: "UPDATE_FOLLOWING_REQUESTS", payload: { followingRequests: data.result.followingRequests } })

                    var property = { ...userProfile }
                    property.user.followersRequests = data.results.followersRequests;
                    setProfile(property)

                    if (data.message == "OK") {
                        refAlertInfo.current.showAlert("Success", "Request cancelled successfully.")
                    }
                }
                HideLoader(true)
            })
            .catch(err => {
                console.log(err)
                HideLoader(true)
            })
    }

    const unfollowUser = (userId) => {
        HideLoader(false)
        fetch(`/unfollow`, {
            method: 'put',
            headers: {
                'Content-Type': 'Application/json',
                'Authorization': 'Bearer ' + sessionStorage.getItem('jwt')
            },
            body: JSON.stringify({
                unfollowId: userid
            })
        })
            .then(res => res.json())
            .then(data => {
                if (data.error == "You must be logged in") {
                    history.push('/signin')
                }
                else {
                    dispatch({ type: "UPDATE", payload: { following: data.result.following, followers: data.result.followers } })
                    sessionStorage.setItem('user', JSON.stringify(data.result))



                    setProfile((prevState) => {

                        let newFollower = prevState.user.followers.filter(item => item !== data.result._id)

                        return {
                            ...prevState,
                            user: {
                                ...prevState.user,
                                followers: newFollower
                            }
                        }
                    })

                    if (data.message == "OK") {
                        refAlertInfo.current.showAlert("Success", "Unfollowed user successfully.")
                    }
                }
                HideLoader(true)
            })
            .catch(err => {
                console.log(err)
                HideLoader(true)
            })
    }

    const toggleFollowersModal = () => {

        if (userProfile && userProfile.user && !userProfile.user.followers.length > 0) {
            refAlertInfo.current.showAlert("Warning", "No followers of the selected user.")
            return;
        }

        HideLoader(false)
        fetch(`/get-followers-list-user/${userid}`, {
            headers: {
                'Authorization': 'Bearer ' + sessionStorage.getItem('jwt')
            }
        })
            .then(res => res.json())
            .then(result => {
                if (result.error == "You must be logged in") {
                    history.push('/signin')
                }
                else {
                    setFollowersListModal(result.user.followers)
                    OpenModal(1)
                }
                HideLoader(true)
            })
            .catch(err => {
                console.log(err)
                HideLoader(true)
            })
    }

    const toggleFollowingModal = () => {

        if (userProfile && userProfile.user && !userProfile.user.following.length > 0) {
            refAlertInfo.current.showAlert("Warning", "Not following anyone.")
            return;
        }


        HideLoader(false)
        fetch(`/get-following-list-user/${userid}`, {
            headers: {
                'Authorization': 'Bearer ' + sessionStorage.getItem('jwt')
            }
        })
            .then(res => res.json())
            .then(result => {
                if (result.error == "You must be logged in") {
                    history.push('/signin')
                }
                else {
                    setFollowingListModal(result.user.following)
                    OpenModal(2)
                }
                HideLoader(true)
            })
            .catch(err => {
                console.log(err)
                HideLoader(true)
            })
    }

    const OpenModal = (modalId) => {
        setStateData((prevState) => {
            return { ...prevState, modalId: modalId };
        });
    };

    const ShowGalleryPictures = () => {
        let columns = [];

        userProfile.posts.forEach((item, idx) => {
            columns.push(
                <Col md="3">
                    <img className='gallery-pictures' key={item._id} alt={item.title} src={item.picture} />
                </Col>
            )
            if ((idx + 1) % 4 === 0) { columns.push(<Row className='m-t'></Row>) }
        })
        return (
            <Row>
                {columns}
            </Row>
        )
    }


    return (
        <div>
            {userProfile ?
                <div style={{ maxWidth: '60%', margin: '0 auto' }}>

                    <Row style={{ margin: '18px 0px', borderBottom: '1px solid grey' }}>
                        <Col md="4">
                            <div>
                                <img className="profile-picture" style={{ width: '160px', height: '160px', borderRadius: '80px' }}
                                    src={userProfile ? userProfile.user.picture : 'loading..'}
                                />
                            </div>

                            <div style={{ textAlign: 'center', marginTop: '15px', marginBottom: '15px' }}>


                            </div>
                        </Col>

                        <Col md="8">
                            <Row style={{ textAlign: 'center' }}>
                                <h5>{userProfile ? userProfile.user.name : "loading"}</h5>
                                <h6>{userProfile ? userProfile.user.email : "loading"}</h6>
                            </Row>

                            <Row className='profile-summary'>
                                <div className="col-sm-3 mlr tac">
                                    <p><strong>{userProfile.posts.length}</strong> Posts</p>
                                </div>
                                <div className="col-sm-3 mlr tac">
                                    <p onClick={toggleFollowersModal}><strong>{userProfile.user.followers.length}</strong> Followers</p>
                                    <div className="row">

                                        {(userProfile && !userProfile.user.followers.includes(state._id) && !userProfile.user.followersRequests.includes(state._id)) && (
                                            <Button
                                                color='primary'
                                                size='sm'
                                                onClick={() => sendFollowRequest()}
                                            >
                                                Follow
                                            </Button>
                                        )}
                                        {(userProfile && userProfile.user.followersRequests.includes(state._id)) && (
                                            <Button
                                                color='secondary'
                                                size='sm'
                                                onClick={() => cancelFollowRequest()}
                                            >
                                                Cancel Request
                                            </Button>
                                        )}
                                        {(userProfile && userProfile.user.followers.includes(state._id)) && (
                                            <Button
                                                color='primary'
                                                size='sm'
                                                onClick={() => unfollowUser()}
                                            >
                                                Un Follow
                                            </Button>
                                        )}
                                    </div>
                                </div>
                                <div className="col-sm-3 mlr tac">
                                    <p onClick={toggleFollowingModal}><strong>{userProfile.user.following.length}</strong> Following</p>
                                </div>
                            </Row>
                        </Col>
                    </Row>

                    {(userProfile && userProfile.user.followers.includes(state._id)) &&
                         ShowGalleryPictures()
                    }
                    {(userProfile && !userProfile.user.followers.includes(state._id)) &&
                        <p>Private Account! No posts to show</p>
                    }
                </div>
                : <h2> loading...</h2>}

            <Modal
                isOpen={stateVariable.modalId == 1}
                toggle={(e) => OpenModal(1)}
                className="com-opt-modal text-center"
                backdrop="static"
            >
                <ModalHeader toggle={(e) => OpenModal(0)}>
                    Followers List
                </ModalHeader>
                <ModalBody>
                    <Row>
                        {followersListModal.length > 0 && (
                            <Card className="h6">
                                <CardBody>

                                    <ul className="collection">
                                        {
                                            followersListModal.map(item => {
                                                return <li key={item._id} className="collection-item style-none">

                                                    {item._id === state._id && (
                                                        <Link to={"/profile"}>
                                                            <img className="popup-image" style={{ width: '30px', height: '30px', borderRadius: '60px', marginRight: '10px' }}
                                                                src={item.picture ? item.picture : 'loading..'}
                                                            />
                                                            {item.name}
                                                        </Link>
                                                    )}
                                                    {item._id !== state._id && (
                                                        <Link to={"/profile/" + item._id}>
                                                            <img className="popup-image" style={{ width: '30px', height: '30px', borderRadius: '60px', marginRight: '10px' }}
                                                                src={item.picture ? item.picture : 'loading..'}
                                                            />
                                                            {item.name}
                                                        </Link>
                                                    )}
                                                </li>
                                            })
                                        }
                                    </ul>

                                </CardBody>
                            </Card>
                        )}
                        {
                            followersListModal.length == 0 && <li key={1} className="collection-item style-none"> No followers </li>
                        }
                    </Row>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={(e) => { OpenModal(0); setFollowersListModal([]) }}>
                        Close
                    </Button>
                </ModalFooter>
            </Modal>



            <Modal
                isOpen={stateVariable.modalId == 2}
                toggle={(e) => OpenModal(2)}
                className="com-opt-modal text-center"
                backdrop="static"
            >
                <ModalHeader toggle={(e) => OpenModal(0)}>
                    Following List
                </ModalHeader>
                <ModalBody>
                    <Row>
                        {followingListModal.length > 0 && (
                            <Card className="h6">
                                <CardBody>
                                    <ul className="collection">
                                        {
                                            followingListModal.map(item => {
                                                return <li key={item._id} className="collection-item style-none">
                                                    {item._id === state._id && (
                                                        <Link to={"/profile"}>
                                                            <img className="popup-image" style={{ width: '30px', height: '30px', borderRadius: '60px', marginRight: '10px' }}
                                                                src={item.picture ? item.picture : 'loading..'}
                                                            />
                                                            {item.name}
                                                        </Link>
                                                    )}
                                                    {item._id !== state._id && (
                                                        <Link to={"/profile/" + item._id}>
                                                            <img className="popup-image" style={{ width: '30px', height: '30px', borderRadius: '60px', marginRight: '10px' }}
                                                                src={item.picture ? item.picture : 'loading..'}
                                                            />
                                                            {item.name}
                                                        </Link>
                                                    )}



                                                </li>

                                                {
                                                    item._id === state._id && (
                                                        <Link to={"/profile"}>
                                                            <img className="popup-image" style={{ width: '30px', height: '30px', borderRadius: '60px', marginRight: '10px' }}
                                                                src={item.picture ? item.picture : 'loading..'}
                                                            />
                                                            {item.name}
                                                        </Link>
                                                    )
                                                }
                                                {
                                                    item._id !== state._id && (
                                                        <Link to={"/profile/" + item._id}>
                                                            <img className="popup-image" style={{ width: '30px', height: '30px', borderRadius: '60px', marginRight: '10px' }}
                                                                src={item.picture ? item.picture : 'loading..'}
                                                            />
                                                            {item.name}
                                                        </Link>
                                                    )
                                                }
                                            })
                                        }
                                    </ul>
                                </CardBody>
                            </Card>
                        )}
                        {
                            followingListModal.length == 0 && <li key={1} className="collection-item style-none"> Not following anyone </li>
                        }
                    </Row>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={(e) => { OpenModal(0); setFollowingListModal([]) }}>
                        Close
                    </Button>
                </ModalFooter>
            </Modal>

            <AlertModal ref={refAlertInfo} />
        </div>
    )
}

export default UserProfile