import React, {useState, useRef} from 'react'
import {Link, useHistory,useParams} from 'react-router-dom'
import {HideLoader, AlertModal} from "../../components/Common/SharedFunctions"
const NewPassword = () => {

    const history = useHistory()
    const [password, setPassword] = useState("")
    const {token} = useParams()
    const refAlertInfo = useRef()

    const postData = () => {
        HideLoader(false)
        fetch('/new-password', {
            method: "post",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                password,
                token
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data.error) {
                refAlertInfo.current.showAlert("Error", data.error)
            }
            else {
                refAlertInfo.current.showAlert("Success", data.data)
                history.push('/signin')
            }
            HideLoader(true)
        })
        .catch(err => {
            console.log(err)
            HideLoader(true)
        })
    }

    return (
        <div className="login-card">
            <div className="card auth-card input-field">
                <h4 className="card-header">Reset Password</h4>
                <input 
                    type="password" 
                    placeholder="Enter a new password" 
                    value={password}
                    autoComplete="none"
                    onChange={(e) => {setPassword(e.target.value)}}
                />
                <button onClick={postData} className="btn waves-effect waves-light #64b5f6 blue darken-1">Update</button>
                <h6>
                    <Link to="/signup">Don't have an account ?</Link>
                </h6>
            </div>

            <AlertModal  ref={refAlertInfo}/>
        </div>
    )
}

export default NewPassword