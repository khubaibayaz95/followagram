import React, { useState, useEffect, useRef } from 'react'
import { Link, useHistory } from 'react-router-dom'
import { HideLoader, AlertModal } from "../../components/Common/SharedFunctions"
import { Row, Button, Col, Input } from "reactstrap";

const Signup = () => {
    const history = useHistory()
    const [name, setName] = useState("")
    const [password, setPassword] = useState("")
    const [email, setEmail] = useState("")
    const [image, setImage] = useState("")
    const [url, setUrl] = useState(undefined)
    const refAlertInfo = useRef()
    const refAlert = useRef()


    useEffect(() => {
        if (url) {
            uploadFields()
        }
    }, [url])

    const uploadPic = () => {
        let data = new FormData()
        data.append("file", image)
        data.append("upload_preset", "InstaClone")     // name of preset in cloudinary
        data.append("cloud_name", "AS")
        HideLoader(false)
        fetch("https://api.cloudinary.com/v1_1/instacloneappudemy/image/upload", {
            method: "post",
            body: data
        })
            .then(res => res.json())
            .then(data => {
                setUrl(data.url)
                HideLoader(true)
            })
            .catch(err => {
                console.log(err)
                HideLoader(true)
            })
    }
    const uploadFields = () => {
        if (!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email)) {
            refAlertInfo.current.showAlert("Error", "Please enter valid email address")
            return
        }
        HideLoader(false)
        fetch('/signup', {
            method: "post",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                name,
                password,
                email,
                picture: url
            })
        })
            .then(res => res.json())
            .then(data => {
                if (data.error) {
                    refAlertInfo.current.showAlert("Error", data.error)
                }
                else {
                    refAlert.current.showAlert("Success", data.message)

                }
                HideLoader(true)
            })
            .catch(err => {
                console.log(err)
                HideLoader(true)
            })
    }
    const postData = () => {

        if (image) {
            uploadPic()
        }
        else {
            uploadFields();
        }
    }

    return (
        <div style={{ maxWidth: '85%', margin: '0 auto' }}>
            <div className="login-card">
                <div className="card auth-card input-field">
                    <h2>Signup</h2>
                    <Row style={{ marginBottom: "5px" }}>
                        <Col sm="12">
                            <Input
                                type="text"
                                placeholder="Name"
                                name="name"
                                maxLength={50}
                                autoComplete="off"
                                value={name}
                                onChange={(e) => { setName(e.target.value) }}
                            />
                        </Col>
                    </Row>
                    <Row style={{ marginBottom: "5px" }}>
                        <Col sm="12">
                            <Input
                                type="text"
                                placeholder="Email"
                                name="email"
                                maxLength={50}
                                autoComplete="off"
                                value={email}
                                onChange={(e) => { setEmail(e.target.value) }}
                            />
                        </Col>
                    </Row>
                    <Row style={{ marginBottom: "5px" }}>
                        <Col sm="12">
                            <Input
                                type="password"
                                placeholder="Password"
                                name="password"
                                maxLength={50}
                                autoComplete="off"
                                value={password}
                                onChange={(e) => { setPassword(e.target.value) }}
                            />
                        </Col>
                    </Row>

                    <Row>
                        <div className="btn custom-btn waves-effect waves-light #64b5f6 blue darken-1">
                            <span>Upload Picture</span>
                            <input type="file" onChange={(e) => setImage(e.target.files[0])} />
                        </div>
                        <div style={{ display: 'none' }} className="file-path-wrapper">
                            <input className="file-path validate" type="text" />
                        </div>
                    </Row>


                    <Row>
                        <Col sm="12">
                            <Button
                                size='sm'
                                color='primary'
                                onClick={() => postData()}
                            >
                                Signup
                            </Button>
                        </Col>
                    </Row>
                </div>
            </div>

            <div className="login-card no-account">
                <div className="card auth-card input-field">
                    <h6>
                        Already have an account? <span><Link to="/signin">Sign in</Link></span>
                    </h6>
                </div>
            </div>

            <AlertModal ref={refAlertInfo} />
            <AlertModal ref={refAlert} hideClose={true} onClose={() => history.push('/signin')} />
        </div>
    )
}

export default Signup