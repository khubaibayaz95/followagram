import React, {useState, useContext, useRef} from 'react'
import {Link, useHistory} from 'react-router-dom'
import {HideLoader, AlertModal} from "../../components/Common/SharedFunctions"

const ResetPassword = () => {

    const history = useHistory()
    const [email, setEmail] = useState("")
    const refAlertInfo = useRef()

    const postData = () => {

        if(!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email)) {
            refAlertInfo.current.showAlert("Invalid", "Please enter valid email address")
            return
        }
        HideLoader(false)
        fetch('/reset-password', {
            method: "post",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                email
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data.error) {
                refAlertInfo.current.showAlert("Error", data.error)
            }
            else {
                refAlertInfo.current.showAlert("Success", data.data)
                history.push('/signin')
            }
            HideLoader(true)
        })
        .catch(err => {
            console.log(err)
            HideLoader(true)
        })
    }

    return (
        <div className="login-card">
            <div className="card auth-card input-field">
                <h4 className="card-header">Reset Password</h4>
                <input 
                    type="text" 
                    placeholder="email" 
                    value={email}
                    autoComplete="none"
                    onChange={(e) => {setEmail(e.target.value)}}
                />
                
                <button onClick={postData} className="btn waves-effect waves-light #64b5f6 blue darken-1">Reset</button>
                <h6>
                    <Link to="/signup">Don't have an account ?</Link>
                </h6>
            </div>

            <AlertModal  ref={refAlertInfo}/>
        </div>
    )
}

export default ResetPassword