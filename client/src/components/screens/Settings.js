import React, { useState, useEffect, useRef, useContext } from "react"
import { useHistory } from "react-router-dom"
import { HideLoader, AlertModal } from "../../components/Common/SharedFunctions"
import { Row, Button, Col, Input, Card, CardBody } from "reactstrap";
import { UserContext } from '../../App'

const Settings = () => {

    const { state, dispatch } = useContext(UserContext)
    const history = useHistory()
    const refAlertInfo = useRef()
    const [settings, setSettings] = useState({
        postsPrivacy: "",
        messagesPrivacy: "",
        postsPrivacyTypeId: "",
        messagesPrivacyTypeId: "",

    })
    const [privacyTypes, setPrivacyTypes] = useState([])


    useEffect(async() => {
        HideLoader(false)

        try {
            
            if(state && state.settingsId !== null && state.settingsId !== undefined && state.settingsId !== '') {
                let settingsId = state.settingsId;

                const [privacyTypesApi, settingsApi] = await Promise.all([
                    fetch("/privacy-types", { headers: { 'Authorization': 'Bearer ' + sessionStorage.getItem('jwt')} }),
                    fetch(`/get-settings/${settingsId}`, { headers: { 'Authorization': 'Bearer ' + sessionStorage.getItem('jwt'),} })
                  ]);

                const privacyTypes = await privacyTypesApi.json();
                const settings = await settingsApi.json();

                if (privacyTypes.error == "You must be logged in") {
                    history.push('/signin')
                }
                else {
                    setPrivacyTypes(privacyTypes.data)
                }

                if (settings.error == "You must be logged in") {
                    history.push('/signin')
                }
                else {

                    setSettings((prevState) => {
                        return {
                            ...prevState,
                            postsPrivacy: settings.data.postsPrivacy.type,
                            postsPrivacyTypeId: settings.data.postsPrivacy._id,
                            messagesPrivacy: settings.data.messagesPrivacy.type,
                            messagesPrivacyTypeId: settings.data.messagesPrivacy._id
                        };
                    });


                }
                HideLoader(true)

          
            }
        } catch (error) {
            HideLoader(true)
        }

        // fetch('/privacy-types', {
        //     headers: {
        //         'Authorization': 'Bearer ' + sessionStorage.getItem('jwt'),
        //         'Content-Type': 'application/json'
        //     }
        // })
        //     .then(res => res.json())
        //     .then(result => {
        //         if (result.error == "You must be logged in") {
        //             history.push('/signin')
        //         }
        //         else {
        //             setPrivacyTypes(result.data)
        //         }
        //         HideLoader(true)
        //     })
        //     .catch(err => {
        //         console.log(err)
        //         HideLoader(true)
        //     })

    }, [])


    const saveSettings = async () => {

        if (state.settingsId === null || state.settingsId === undefined || state.settingsId === '') {
            HideLoader(false)
            fetch('/add-new-settings', {
                method: "post",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + sessionStorage.getItem('jwt')
                },
                body: JSON.stringify({
                    userId: state._id,
                    postsPrivacy: settings.postsPrivacyTypeId,
                    messagesPrivacy: settings.messagesPrivacyTypeId,
                    settingsId: ""
                })
            })
                .then(res => res.json())
                .then(data => {
                    if (data.error) {
                        refAlertInfo.current.showAlert("Invalid", data.error)
                    }
                    else {
                        dispatch({ type: "INSERT_SETTINGS_ID", payload: data.data._id })
                        sessionStorage.setItem('user', JSON.stringify({ ...state, settingsId: data.data._id }))

                        refAlertInfo.current.showAlert("Success", 'Settings saved successfully')
                    }
                    HideLoader(true)
                })
                .catch(err => {
                    HideLoader(true)

                    console.log(err)
                })
        }

    }


    const handleChanges = (event) => {
        try {
            if (event.target.name === 'PostPrivacy') {
                let value = event.target.value;
                var id = event.target.selectedOptions[0].id;
                setSettings((prevState) => {
                    return {
                        ...prevState,
                        postsPrivacy: value,
                        postsPrivacyTypeId: id
                    };
                });
            }
            else if (event.target.name === 'MessagesPrivacy') {
                let value = event.target.value;
                var id = event.target.selectedOptions[0].id;
                setSettings((prevState) => {
                    return {
                        ...prevState,
                        messagesPrivacy: value,
                        messagesPrivacyTypeId: id
                    };
                });
            }
        } catch (e) { }
    }

    return (
        <div className="tac">
            <Card
                style={{ fontSize: "14px", maxWidth: '50%', margin: '10px' }}
                className="mt-5 mx-auto"
                id="myNewForm"
            >
                <CardBody
                    className="top-border"
                    style={{
                        paddingTop: "0.6rem",
                        paddingBottom: "0.6rem",
                        paddingLeft: "1rem",
                        paddingRight: "1rem",
                    }}
                >

                    <Row className="mb5">
                        <Col lg="3">
                            <h6 className="transformY-30"> Who can view posts</h6>
                        </Col>
                        <Col lg="6">
                            <select
                                name="PostPrivacy"
                                className="form-control input-sm"
                                onChange={(e) => handleChanges(e)}
                                value={settings.postsPrivacy}
                            >
                                <option value="" id="">
                                    Select Privacy
                                </option>
                                {privacyTypes ? privacyTypes.map(
                                    (val, key) => {
                                        return (
                                            <option
                                                key={key}
                                                id={val._id}
                                                value={val.type}
                                            >
                                                {val.type}
                                            </option>
                                        );
                                    }
                                ) : []}
                            </select>
                        </Col>
                        <Col lg="3"></Col>
                    </Row>
                    <Row className="mb5">
                        <Col lg="3">
                            <h6 className="transformY-30"> Who can send messages</h6>
                        </Col>
                        <Col lg="6">
                            <select
                                name="MessagesPrivacy"
                                className="form-control input-sm"
                                onChange={(e) => handleChanges(e)}
                                value={settings.messagesPrivacy}
                            >
                                <option value="" id="">
                                    Select Privacy
                                </option>

                                {privacyTypes ? privacyTypes.map(
                                    (val, key) => {
                                        return (
                                            <option
                                                key={key}
                                                id={val._id}
                                                value={val.type}
                                            >
                                                {val.type}
                                            </option>
                                        );
                                    }
                                ) : []}
                            </select>
                        </Col>
                        <Col lg="3"></Col>
                    </Row>

                    <Row>
                        <Col sm="12">
                            <Button
                                size='sm'
                                color='primary'
                                onClick={() => saveSettings()}
                            >
                                Save
                            </Button>
                        </Col>
                    </Row>
                    <AlertModal ref={refAlertInfo} />
                </CardBody>
            </Card>
        </div>
    );
}

export default Settings