import React, { useState, useContext, useRef } from 'react'
import { Link, useHistory } from 'react-router-dom'
import { UserContext } from '../../App'
import { HideLoader, AlertModal } from "../Common/SharedFunctions"
import { Row, Button, Col, Input } from "reactstrap";


const Signin = () => {

    const { state, dispatch } = useContext(UserContext)
    const history = useHistory()
    const [password, setPassword] = useState("")
    const [email, setEmail] = useState("")
    const refAlertInfo = useRef()

    const postData = () => {

        if (!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email)) {
            refAlertInfo.current.showAlert("Invalid", "Please enter valid email address.")
            return
        }
        HideLoader(false)
        fetch('/signin', {
            method: "post",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                password,
                email
            })
        })
            .then(res => res.json())
            .then(data => {
                if (data.error) {
                    refAlertInfo.current.showAlert("Invalid", data.error)
                }
                else {
                    sessionStorage.setItem('jwt', data.token)
                    sessionStorage.setItem('user', JSON.stringify(data.user))

                    dispatch({ type: "USER", payload: data.user })
                    history.push('/')
                }
                HideLoader(true)
            })
            .catch(err => {
                console.log(err)
                HideLoader(true)

            })
    }

    return (
        <div style={{ maxWidth: '85%', margin: '0 auto' }}>
            <div className="login-card">
                <div className="card auth-card input-field">
                    <h2>Signin</h2>
                    <Row style={{ marginBottom: "5px" }}>
                        <Col sm="12">
                            <Input
                                type="text"
                                placeholder="Email"
                                name="email"
                                maxLength={50}
                                autoComplete="off"
                                value={email}
                                onChange={(e) => { setEmail(e.target.value) }}
                            />
                        </Col>
                    </Row>
                    <Row style={{ marginBottom: "5px" }}>
                        <Col sm="12">
                            <Input
                                type="password"
                                placeholder="Password"
                                name="password"
                                maxLength={50}
                                autoComplete="off"
                                value={password}
                                onChange={(e) => { setPassword(e.target.value) }}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="12">
                            <Button
                                size='sm'
                                color='primary'
                                onClick={() => postData()}
                            >
                                Signin
                            </Button>
                        </Col>

                    </Row>
                    <p>
                        <Link to="/reset-password">Forgot Password ?</Link>
                    </p>
                </div>
            </div>

            <div className="login-card no-account">
                <div className="card auth-card input-field">
                    <h6>
                        Don't have an account? <span><Link to="/signup">Sign up</Link></span>
                    </h6>
                </div>
            </div>

            <AlertModal ref={refAlertInfo} />
        </div>
    )
}

export default Signin