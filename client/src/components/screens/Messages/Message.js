import "./message.css"
import {format} from "timeago.js"

const Message = ({message, own, imgSrc}) => {
    return (
        <div className={own ? "message own" : "message"}>
            <div className="messageTop">
                <img 
                    className="messageImg" 
                    src={own ? imgSrc : imgSrc}
                    alt="" 
                />

{/* src={own ? imgSrc : "https://dt2sdf0db8zob.cloudfront.net/wp-content/uploads/2019/12/9-Best-Online-Avatars-and-How-to-Make-Your-Own-for-Free-image1-5.png"} */}

                <p className="messageText">{message?.text}</p>
            </div>
            <div className="messageBottom">
                {format(message?.createdAt)}
            </div>
        </div>
    )
}

export default Message;