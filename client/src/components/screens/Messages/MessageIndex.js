import { useState, useContext, useEffect, useRef } from "react"
import ChatOnline from "./ChatOnline"
import { useHistory } from 'react-router-dom'
import Conversation from "./Conversation"
import Message from "./Message"
import { UserContext, SocketContext } from '../../../App'
import "./messageIndex.css"
import {HideLoader, AlertModal} from "../../Common/SharedFunctions"

const MessageIndex = () => {

    const history = useHistory();
    const [conversations, setConversations] = useState([])
    const [currentChat, setCurrentChat] = useState(null)
    const [currentChatUserImg, setCurrentChatUserImg] = useState(null)
    const [newChat, setNewChat] = useState(false)
    const [newChatUser, setNewChatUser] = useState(null)

    const [messages, setMessages] = useState([])
    const [newMessage, setNewMessage] = useState("")
    const [arrivalMessage, setArrivalMessage] = useState(null)
    const [onlineUsers, setOnlineUsers] = useState([])
    const refAlertInfo = useRef()
    //const socket = useRef(io("ws://localhost:8900"))

    const { state, dispatch } = useContext(UserContext)
    const socket = useContext(SocketContext)

    const scrollRef = useRef();
    const conversationRef = useRef();

    // useEffect(() => {

    //     // for not connecting again and again after reloading Messenger component
    //     //socket.current = io("ws://localhost:5000");
    //     socket.current = io();



    //     return() => {
    //         // Closing the socket connection when leaving the messenger screen so that friends can't see online
    //         socket.current.close();
    //     }

    // }, [])

    useEffect(() => {
        if(state !== null & state !== undefined) {
            if(socket && socket.current) {
                socket.current.emit("getOnlineUsers");
                socket.current.on("getUsers", users => {
                    setOnlineUsers(state.following.filter(f => users.some(u => u.userId === f)))
                })
            }            
        }
            
        
    }, [state])

    useEffect(() => {
        if(socket && socket.current) {
            socket.current.on("getMessage", data => {
                setArrivalMessage({
                    sender: data.senderId,
                    text: data.text,
                    createdAt: Date.now()
                })
            })
        }
    }, [])

    useEffect(() => {
        arrivalMessage && currentChat?.members.includes(arrivalMessage.sender) &&
            setMessages(prev => [...prev, arrivalMessage])
    }, [arrivalMessage, currentChat])

    useEffect(() => {
        const getConversations = async() => {
            try{                
                HideLoader(false)
                fetch('/getConversation/' + state._id, {
                    headers: {
                        'Authorization': 'Bearer ' + sessionStorage.getItem('jwt')
                    }
                })
                .then(res => res.json())
                .then(result => {
                    if (result.error == "You must be logged in") {
                        history.push('/signin')
                    }
                    else {
                        setConversations(result.data);
                    }
                    HideLoader(true)
                })
                .catch(err => {
                    console.log(err)
                    HideLoader(true)
                })
            }
            catch(err) {
                console.log(err)
            }
        }
        getConversations();
    }, [state?._id])

    useEffect(() => {
        const getMessages = async() => {
            try{
                HideLoader(false)
                
                await fetch(`/message/get-msg-from-conv/` + currentChat?._id, {
                    headers: {
                        'Authorization': 'Bearer ' + sessionStorage.getItem('jwt')
                    }
                })
                .then(res => res.json())
                .then(result =>{
                    if(result.error == "You must be logged in") {
                        history.push('/signin')
                    }
                    else {
                        setMessages(result.data);
                    }
                   HideLoader(true)
                })
                .catch(err => {
                    console.log(err)
                   HideLoader(true)
                })
            }
            catch(err) {
                console.log(err)
               HideLoader(true)
            }
        }
        if(currentChat !== null && !newChat) {
            getCurrentUserImg()
            getMessages();
        }
    }, [currentChat])

    const getCurrentUserImg = async() => {
        try{
            
            const friendId = currentChat.members.find((m) => m !== state?._id)
            HideLoader(false)
            await fetch(`/user/get-user-details/${friendId}`, {
                headers: {
                    'Authorization': 'Bearer ' + sessionStorage.getItem('jwt')
                }
            })
            .then(res => res.json())
            .then(result =>{
                if(result.error == "You must be logged in") {
                    history.push('/signin')
                }
                else {
                    if(result.data.picture !== undefined && result.data.picture !== null) {
                        setCurrentChatUserImg(result.data.picture)
                    }
                }
                HideLoader(true)
            })
            .catch(err => {
                console.log(err)
                setCurrentChatUserImg(null)
                HideLoader(true)
            })
        }
        catch(err) {
            console.log(err)
            setCurrentChatUserImg(null)
        }
    }

    const handleSubmit = async (e) => {
        e.preventDefault();

        const receiverId = currentChat.members.find((member) => member !== state?._id)

        if(socket && socket.current) {
            socket.current.emit("sendMessage", {
                senderId: state?._id,
                receiverId,
                text: newMessage
            })
        }

        try{
            await fetch('/message/sendMessage', {
                method: "post",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + sessionStorage.getItem('jwt')
                },
                body: JSON.stringify({
                    sender: state?._id,
                    text: newMessage,
                    conversationId: currentChat?._id
                })
            })
            .then(res => res.json())
            .then(data => {
                if(data.error) {
                    refAlertInfo.current.showAlert("Error", data.error)
                }
                if(data.error == "You must be logged in") {
                    history.push('/signin')
                }
                else {
                    setMessages([...messages, data.data])
                    setNewMessage("");
                }
            })
            .catch(err => {
                refAlertInfo.current.showAlert("Error", err)
            })
        }
        catch(err) {
            refAlertInfo.current.showAlert("Error", err)
        }
    }


    useEffect(() => {
        scrollRef.current?.scrollIntoView({ behavior: "smooth" })
    }, [messages])

    useEffect(() => {
        
        if(newChat === true && newChatUser !== null) {
            try{
                HideLoader(false)
                fetch('/createConversation', {
                    method: "post",
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": "Bearer " + sessionStorage.getItem('jwt')
                    },
                    body: JSON.stringify({
                        senderId: state?._id,
                        receiverId: newChatUser,
                    })
                })
                .then(res => res.json())
                .then(result => {
                    if(result.error) {
                        refAlertInfo.current.showAlert("Error", result.error)
                    }
                    if(result.error == "You must be logged in") {
                        history.push('/signin')
                    }
                    else {
                        setConversations([...conversations, result.data])
                        conversationRef.current.click();

                        setNewChat(false)
                        setNewChatUser(null)
                    }
                    HideLoader(true)
                })
                .catch(err => {
                    refAlertInfo.current.showAlert("Error", err)
                    HideLoader(true)
                })
            }
            catch(err) {
                refAlertInfo.current.showAlert("Error", err)
            }
        }

    }, [newChatUser, newChat])



    return (
        <>
            <div className="messenger">
                <div className="chatMenu">
                    <div className="chatMenuWrapper">
                        <input placeholder="Search for friends" className="chatMenuInput" />
                        {conversations.map((c) => (
                            <div ref={conversationRef} onClick={() =>{
                                setCurrentChat(c)
                                setNewChat(false)
                            } }>
                                <Conversation conversation={c} currentUser={state?._id} />
                            </div>
                        ))}
                        
                    </div>
                </div>
                
                <div className="chatBox">
                    <div className="chatBoxWrapper">
                        {
                            currentChat ?
                            <>
                                <div className="chatBoxTop">
                                    {messages.map((m) => (
                                        <div ref={scrollRef}>
                                            {/* <Message key={m} message={m} own={m.sender === user?._id} />  */}
                                            <Message imgSrc={m.sender === state?._id ? state?.picture : currentChatUserImg} key={m} message={m} own={m.sender === state?._id} /> 
                                        </div>
                                    ))}
                                </div>
                                <div className="chatBoxBottom">
                                    <textarea 
                                        className="chatMessageInput" 
                                        placeholder="Type here..." 
                                        onChange={(e) => setNewMessage(e.target.value)}
                                        value={newMessage}
                                    />
                                    <button className="chatSubmitButton" onClick={handleSubmit}> Send </button>
                                </div> 
                            </> : 
                            <span className="noConversationText">Open a conversation to start chatting</span>
                        }
                    </div>
                </div>
                <div className="chatOnline">
                    <div className="chatOnlineWrapper">
                        <ChatOnline 
                            onlineUsers={onlineUsers} 
                            currentId={state?._id} 
                            setCurrentChat={setCurrentChat}
                            setNewChat={setNewChat}
                            setNewChatUser={setNewChatUser}
                        />
                    </div>
                </div>


                <AlertModal  ref={refAlertInfo}/>
            </div>
        </>
    )
}

export default MessageIndex