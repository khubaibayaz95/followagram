import {useState, useEffect} from "react"
import "./chatOnline.css"
import { useHistory } from 'react-router-dom'
import {HideLoader} from "../../Common/SharedFunctions"

const ChatOnline = ({onlineUsers, currentId, setCurrentChat, setNewChat, setNewChatUser}) => {
    
    const [friends, setFriends] = useState([])
    const [onlineFriends, setOnlineFriends] = useState([])
    const [allFriends, setAllFriends] = useState([])

    const PF = process.env.REACT_APP_PUBLIC_FOLDER
    const history = useHistory();

    useEffect(() => {

        const getFriends = async() => {
            HideLoader(false)
            fetch('/user/friends/' + currentId, {
                headers: {
                    'Authorization': 'Bearer ' + sessionStorage.getItem('jwt')
                }
            })
            .then(res => res.json())
            .then(result => {
                if (result.error == "You must be logged in") {
                    history.push('/signin')
                }
                else {
                    setFriends(result.data)
                }
                HideLoader(true)
            })
            .catch(err => {
                console.log(err)
                HideLoader(true)
            })
        }
        if(currentId !== undefined && currentId !== null) {
            getFriends();
        }
    }, [currentId])

    useEffect(() => {
        if(friends) {
            setOnlineFriends(friends.filter((f) => onlineUsers.includes(f._id)));

            let AllUsers = friends.filter((f) => !onlineUsers.includes(f._id));
            if(AllUsers && AllUsers.length > 0) {
                AllUsers.sort(function(a,b){
                    if(a.name.toLowerCase() < b.name.toLowerCase()) { return -1; }
                    if(a.name.toLowerCase() > b.name.toLowerCase()) { return 1; }
                    return 0;
                });
                
                setAllFriends(AllUsers)
            }
        }
    }, [friends, onlineUsers])


    const handleClick = async (user) => {
        try {
            HideLoader(false)
            fetch(`/findConversation/${currentId}/${user._id}`, {
                headers: {
                    'Authorization': 'Bearer ' + sessionStorage.getItem('jwt')
                }
            })
            .then(res => res.json())
            .then(result => {
                if(result.data !== null) {
                    setCurrentChat(result.data);
                    setNewChat(false)
                }
                else {
                    setNewChatUser(user._id)
                    setNewChat(true)
                }
                HideLoader(true)
            })
        }
        catch(err) {
            console.log(err)
            HideLoader(true)
        }
    }

    return (
        <div className="chatOnline">
            {onlineFriends.map(o => (
                <div className="chatOnlineFriend" onClick={() => {
                    handleClick(o)
                }}>
                <div className="chatOnlineImgContainer">
                    <img 
                        className="chatOnlineImg"
                        src={o?.picture ? o.picture : PF+"person/noAvatar.png"} 
                        alt="" 
                    />
                    {onlineUsers.includes(o._id) && ( 
                        <div className="chatOnlineBadge"></div>
                    )}
                </div>
                <span className="chatOnlineName">{o.name}</span>
            </div>
            ))}


            <hr></hr>
            {allFriends.map(o => (
                <div className="chatOnlineFriend" onClick={() => {
                    handleClick(o)
                }}>
                <div className="chatOnlineImgContainer">
                    <img 
                        className="chatOnlineImg"
                        src={o?.picture ? o.picture : PF+"person/noAvatar.png"} 
                        alt="" 
                    />
                    {onlineUsers.includes(o._id) && ( 
                        <div className="chatOnlineBadge"></div>
                    )}
                </div>
                <span className="chatOnlineName">{o.name}</span>
            </div>
            ))}
            
        </div>
    )
}

export default ChatOnline;