import {useState, useEffect} from "react"
import { useHistory } from 'react-router-dom'
import "./conversation.css"
import {HideLoader} from "../../Common/SharedFunctions"

const Conversation = ({conversation, currentUser}) => {

    const history = useHistory();
    const [user, setUser] = useState(null)
    const PF = process.env.REACT_APP_PUBLIC_FOLDER

    useEffect(() => {
        const friendId = conversation.members.find((m) => m !== currentUser)

        const getUser = async() => {
            try{
                HideLoader(false)
                await fetch(`/user/get-user-details/${friendId}`, {
                    headers: {
                        'Authorization': 'Bearer ' + sessionStorage.getItem('jwt')
                    }
                })
                .then(res => res.json())
                .then(result =>{
                    if(result.error == "You must be logged in") {
                        history.push('/signin')
                    }
                    else {
                        setUser(result.data)
                    }
                    HideLoader(true)
                })
                .catch(err => {
                    console.log(err)
                    HideLoader(true)
                })
            }
            catch(err) {
                console.log(err)
            }
        }
        getUser();

    }, [currentUser, conversation])


    return (
        <div className="conversation">
        <img className="conversationImg" 
                 src={user?.picture ? user.picture : PF + "person/noAvatar.png"}
                 alt="" 
             />
             {/* src="https://dt2sdf0db8zob.cloudfront.net/wp-content/uploads/2019/12/9-Best-Online-Avatars-and-How-to-Make-Your-Own-for-Free-image1-5.png"  */}
                
             <span className="conversationName"> {user ? user.name : ""} </span>
        </div>
    )
}

export default Conversation