
export const initialSocketState = null;

export const socketReducer = (state, action) => {
    if(action.type == "SOCKET_INIT") {
        return action.payload
    }
    if(action.type == "SOCKET_CLOSE") {
        return null
    }
    
    return state
}
