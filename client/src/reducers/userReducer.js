
export const initialState = null;

export const reducer = (state, action) => {

    if(action.type == "USER") {
        return action.payload
    }
    if(action.type == "UPDATE") {
        return {...state,
            followers: action.payload.followers,
            following: action.payload.following
        }
    }
    if(action.type == "UPDATEDETAILS") {
        return {...state,
            followers: action.payload.followers,
            following: action.payload.following,
            picture: action.payload.picture
        }
    }
    if(action.type == "UPDATE_FOLLOWING_REQUESTS") {
        return {...state,
            followingRequests: action.payload.followingRequests,
        }
    }
    if(action.type == "UPDATE_FOLLOWERS_REQUESTS") {
        return {...state,
            followersRequests: action.payload.followersRequests,
        }
    }
    if(action.type == "UPDATE_ACCEPT_FOLLOW_REQUESTS") {
        return {...state,
            followersRequests: action.payload.followersRequests,
            followers: action.payload.followers,
        }
    }
    if(action.type == "UPDATEPIC") {
        return {...state,
            picture: action.payload
        }
    }
    if(action.type == "INSERT_SETTINGS_ID") {        
        return {...state,
            settingsId: action.payload
        }
    }
    if(action.type == "LOGOUT") {
        return null
    }
    return state
}
