module.exports = {
    MONGOURI: process.env.MONGOURI,
    JWT_SECRET: process.env.JWT_SEC,
    EMAIL_KEY: process.env.EMAIL_KEY,
    SITE_URL: process.env.SITE_URL
}