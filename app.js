const express = require('express')
const app = express()
const mongoose = require('mongoose')
const PORT = process.env.PORT || 5000;
const {MONGOURI} = require('./config/keys')
const socket = require("socket.io");

// const customMiddleware = (req, res, next) => {
//     console.log('middleware executed')
//     next()
// }

mongoose.connect(MONGOURI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
})
mongoose.connection.on('connected', () => {
    console.log('connected to mongodb')
})
mongoose.connection.on('error', (err) => {
    console.log('error connection to mongodb ', err)
})

require('./models/user')
require('./models/post')
require('./models/conversation')
require('./models/message')
require('./models/setting')
require('./models/privacytype')



app.use(express.json())
app.use(require('./routes/auth'))
app.use(require('./routes/post'))
app.use(require('./routes/user'))
app.use(require('./routes/conversation'))
app.use(require('./routes/message'))
app.use(require('./routes/setting'))
// app.use(require('./routes/privacytypes'))




if(process.env.NODE_ENV == 'production') {

    // serve the static files
    app.use(express.static('client/build'))
    let path = require('path')

    // if client is sending any request we will send index.html all the logic is here (routes etc)
    app.get('*', (req, res) => {
        res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'))
    })
}

const server = app.listen(PORT, () => {
    console.log('Server is running on ', PORT)
})

const io = require("socket.io")(server, {
    cors: {
        origin: "*"
    }
  });
  

  let users = [];


  const addUser = (userId, socketId) => {
    !users.some(user => user.userId === userId) &&
        users.push({userId, socketId})
  }
  
  const removeUser = (socketId) => {
    users = users.filter((user) => user.socketId !== socketId)
  }
  
  const getUser = (userId) => {
    return users.find((user) => user.userId === userId)
  }
  
  io.on("connection", (socket) => {  
    
    //take userId and socketId from user
    socket.on("addUserToSocket", (userId) => {
        addUser(userId, socket.id)
        io.emit("getUsers", users)
        console.log("a user connected - " + userId + ' - '+ socket.id);
    })
    
    //return connected users
    socket.on("getOnlineUsers", () => {
        console.log('on getOnlineUsers called')
        io.emit("getUsers", users)
    })
  
    // SEND and GET message
    socket.on("sendMessage", ({senderId, receiverId, text}) => {

        const user = getUser(receiverId);
        if(user !== undefined) {
            io.to(user.socketId).emit("getMessage", {
                senderId,
                text
            })
            console.log('emit getMessage called')
        }
    })
  
    // When user disconnect
    socket.on("disconnect", () => {
        console.log("a user disconnected " + socket.id)
  
        removeUser(socket.id)
        io.emit("getUsers", users)
    })
    
  })
  